<?php $routes = model('route')->getByTypeId(get_field('village_routes', 'option'))->posts;
//$cities = model('city')->all()->posts; ?>

<header>
    <div class="container">
        <div class="left">
            <?php include_view('layouts/includes/burger.php'); ?>

            <?php include_view('layouts/includes/lang-switcher.php', ['page' => $page]); ?>
        </div>

<!--        --><?php //if (!empty($cities)) : ?>
<!--            <div class="right">-->
<!--                <a href="#" class="choose-m">--><?php //_e('Обрати місто'); ?><!--</a>-->
<!--                <ul class="dropdown__m">-->
<!--                    --><?php //foreach ($cities as $city) : ?>
<!--                        <li>-->
<!--                            <a href="--><?php //echo get_permalink($city); ?><!--">--><?php //echo $city->post_title; ?><!--</a>-->
<!--                        </li>-->
<!--                    --><?php //endforeach; ?>
<!--                </ul>-->
<!--            </div>-->
<!--        --><?php //endif; ?>
    </div>
</header>

<?php include_view('layouts/includes/logos.php'); ?>

<a href="<?php echo qtranxf_get_url_for_language('/', qtranxf_getLanguage()); ?>" class="back"><?php _e('Повернутися на початок пошуку'); ?></a>

<div class="text-block">
    <?php echo wpautop($page->post_content); ?>
</div>

<?php if (!empty($routes)): ?>
    <div class="five-choose choose" id="choose">
        <ul>
            <?php foreach ($routes as $route) : ?>
                <li>
                    <a href="<?php echo get_permalink($route); ?>" style="background-image: url('<?php echo !empty(get_field('route_image', $route)) ? get_field('route_image', $route)['url'] : ''; ?>');"><?php echo $route->post_title; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <a href="#choose" class="load-more"><?php _e('Показати всi'); ?></a>
    </div>
<?php endif; ?>

<?php $about = get_field('about', 'option');

if ($about) : ?>
    <section class="about-project">
        <hr>
        <div class="container">
            <p class="title">
                <?php echo $about->post_title; ?>
            </p>

            <p class="description">
                <?php echo $about->post_content; ?>
            </p>
        </div>
    </section>
<?php endif; ?>

<?php include_view('layouts/sections/footer.php'); ?>

<?php include_view('layouts/includes/how-to-use.php'); ?>
