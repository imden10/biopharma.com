module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `
					@import "~@/scss/mixins/_mediaq.scss";
					@import "~@/scss/mixins/_divs.scss";
					@import "~@/scss/mixins/_colors.scss";
					@import "~@/scss/mixins/_mixinsText.scss";
				`,
            },
        },
    },
    devServer: {
        proxy: {
            '^/api': {
                target: 'https://biopharma.ua/',
                // target: 'http://biopharma.owlweb.com.ua/',
                changeOrigin: true,
                secure: false,
                pathRewrite: {
                    '^/api': '/api'
                },
                logLevel: 'debug'
            },
        }
    },
    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../public/assets/app',

    publicPath: process.env.NODE_ENV === 'production' ?
        '/assets/app/' : '/',

    // modify the location of the generated HTML file.
    indexPath: process.env.NODE_ENV === 'production' ?
        '../../../resources/views/front/home/frontend.blade.php' : 'index.html',

    pluginOptions: {
        i18n: {
            locale: 'uk',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: true
        }
    }
};
