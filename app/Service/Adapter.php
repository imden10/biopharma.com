<?php


namespace App\Service;

use App\Models\BlogArticles;
use App\Models\Langs;
use App\Models\Menu;
use App\Models\Pages;
use App\Modules\Widgets\Models\Widget;

class Adapter
{
    private $defaultLangCode;

    public function __construct()
    {
        $this->defaultLangCode = Langs::getDefaultLangCode();
    }

    private static function cmp($a, $b)
    {
        return $a['position'] - $b['position'];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelResults($model, $lang, $textCountSymbols = null): array
    {
        $translate   = $model->getTranslation($lang);
        $constructor = $model->getTranslation($lang)->constructor->data;

        $model = $model->toArray();

        if(isset($model['category_slug']) && isset($model['category_name'])){
            $url = '/news/' . $model['category_slug'];

            if($this->defaultLangCode !== $lang){
                $url = '/' . $lang . $url;
            }

            $model['category'] = [
                'url'   => $url,
                'title' => $model['category_name']
            ];

            $model['url'] = $url . '/' . $model['slug'];
        }

        unset($model['translations']);
        unset($translate['constructor']);

        if (!is_array(current($constructor)))
            $constructor = [];

        if (count($constructor)) {
            usort($constructor, array('App\Service\Adapter', 'cmp'));

            $allWidgets     = Widget::query()->where('lang', $lang)->pluck('data', 'id')->toArray();
            $allWidgetsName = Widget::query()->where('lang', $lang)->pluck('instance', 'id')->toArray();

            foreach ($constructor as $key => $item) {
                if ($item['component'] === 'widget') {
                    if (isset($allWidgetsName[$item['content']['widget']])) {
                        $constructor[$key]['content']['instance'] = $allWidgetsName[$item['content']['widget']];
                        $widgetClassName                          = config('widgets.widgets')[$allWidgetsName[$item['content']['widget']]];
                        $widgetClass                              = app($widgetClassName);

                        if (method_exists($widgetClass, 'adapter')) {
                            $constructor[$key]['content']['data'] = $widgetClass->adapter($allWidgets[$item['content']['widget']], $lang);
                        } else {
                            $constructor[$key]['content']['data'] = $allWidgets[$item['content']['widget']];
                        }

                        $constructor[$key]['component'] = $constructor[$key]['content']['instance'];
                        $constructor[$key]['content']   = $constructor[$key]['content']['data'];
                        $constructor[$key]['widget_id'] = 'widget_' . $constructor[$key]['component'] . '_' . $item['content']['widget'];

                        if(isset($constructor[$key]['content']['theme']) && $constructor[$key]['content']['theme'] === 'dark'){
                            $constructor[$key]['content']['theme'] = 'gray';
                        }

                        if(isset($constructor[$key]['content']['text']) && $constructor[$key]['content']['text'] === '<p><br></p>') {
                            $constructor[$key]['content']['text'] = "";
                        }

                    } else {
                        unset($constructor[$key]);
                    }
                } elseif ($item['component'] === 'see-also') {
                    $list  = [];
                    $ids   = [];
                    $names = [];

                    foreach ($item['content']['list'] as $l) {
                        $ids[] = (int)$l['page_id'];

                        $names[$l['page_id']] = $l['name'];
                    }

                    if (count($ids)) {
                        $pages = Pages::query()
                            ->active()
                            ->whereIn('id', $ids)
                            ->get()
                            ->toArray();

                        if (count($pages)) {
                            foreach ($ids as $id) {
                                foreach ($pages as $pageKey => $page) {
                                    if ($page['id'] == $id) {
                                        $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_PAGE]['url_prefix'] . $page['path'];

                                        if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                            $url = '/' . $lang . '/' . $url;
                                        } else {
                                            $url = '/' . $url;
                                        }

                                        $list[$pageKey]['url']   = $url;
                                        $list[$pageKey]['title'] = $names[$id];
                                    }

                                }
                            }

                            $constructor[$key]['content']['list'] = array_values($list);
                        }
                    }
                }
            }
        }

        if(isset($translate['text']) && $textCountSymbols){
            $translate['text'] = mb_strimwidth(strip_tags($translate['text']), 0, $textCountSymbols, "...");
        }

        return [
            'model'       => $model,
            'translate'   => $translate,
            'constructor' => array_values($constructor)
        ];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareProductResults($model, $lang): array
    {
        $translate = $model->getTranslation($lang);

        $model = $model->toArray();

        $anchorMenu = $this->getAnchorMenuFromDesc($translate);

        $translate['anchor_menu'] = $anchorMenu;

        if(isset($model['categorySlug']) && isset($model['categoryName'])){
            $url = '/preparati/' . $model['categorySlug'];

            if($this->defaultLangCode !== $lang){
                $url = '/' . $lang . $url;
            }

            $model['url'] = $url . '/' . $model['slug'];
            $model['categoryUrl'] = $url;
        }

        if(isset($translate['package'])){
            $package = [];

            $temp = json_decode($translate['package'],true);

            if(is_array($temp) && count($temp)){
                foreach ($temp as $t){
                    $package[] =   $t['val'];
                }
            }

            $translate['package'] = $package;
        }

        if(isset($translate['indication'])){
            $indication = [];

            $temp = json_decode($translate['indication'],true);

            if(is_array($temp) && count($temp)){
                foreach ($temp as $t){
                    $indication[] =   $t['val'];
                }
            }

            $translate['indication'] = $indication;
        }

        if(isset($translate['catalogIndication'])){
            $catalogIndication= [];

            $temp = json_decode($translate['catalogIndication'],true);

            if(is_array($temp) && count($temp)){
                foreach ($temp as $t){
                    $catalogIndication[] =   $t['val'];
                }
            }

            $translate['catalogIndication'] = $catalogIndication;
        }

        if(isset($translate['contraindications'])){
            $contraindications = [];

            $temp = json_decode($translate['contraindications'],true);

            if(is_array($temp) && count($temp)){
                foreach ($temp as $t){
                    $contraindications[] =   $t['val'];
                }
            }

            $translate['contraindications'] = $contraindications;
        }

        if(isset($translate['publication'])){
            $publication = [];

            $temp = json_decode($translate['publication'],true);

            if(is_array($temp) && count($temp)){
                $temp = array_reverse($temp);
                foreach ($temp as $t){
                    $publication[] = $t;
                }
            }

            $translate['publication'] = $publication;
        }

        unset($model['translations']);

        return [
            'model'     => $model,
            'translate' => $translate,
        ];
    }

    public function prepareProductsResults($model, $lang): array
    {
        $models = [];

        foreach ($model as $item) {
            $models[] = $this->prepareProductResults($item, $lang);
        }

        return [
            'models' => $models
        ];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelsResults($model, $lang, $textCountSymbols = null): array
    {
        $models = [];

        foreach ($model as $item) {
            $models[] = $this->prepareModelResults($item, $lang, $textCountSymbols);
        }

        return [
            'models' => $models
        ];
    }

    public function prepareMenuResults($model, $lang): array
    {
        $res = [];

        $res = $this->getChildrenMenu($model, $lang, $res);

        return $res;
    }

    public function getChildrenMenu($children = null, $lang, $res = [])
    {
        foreach ($children as $key => $item) {
            if ($item['type'] == Menu::TYPE_ARBITRARY) {
                $res[$key]['name'] = $item['name'];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $res[$key]['name'] = $trans['name'];
                        $res[$key]['url']  = $trans['url'];
                        $res[$key]['slug'] = null;
                    }
                }
            } else {
                $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                if (is_null($modelRel)) {
                    continue;
                }

                $modelSlug = $modelRel['slug'];

                if($item['type'] == Menu::TYPE_PAGE){
                    $modelSlug = $modelRel['path'];
                }

                $res[$key]['url'] = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelSlug;
                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                    $res[$key]['url'] = '/' . $lang . '/' . $res[$key]['url'];
                } else {
                    $res[$key]['url'] = '/' . $res[$key]['url'];
                }

                $res[$key]['name'] = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                foreach ($modelRel['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $res[$key]['name'] = $trans[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                        $res[$key]['slug'] = $modelRel['slug'];
                    }
                }

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        if (!empty($trans['name'])) {
                            $res[$key]['name'] = $trans['name'];
                        }
                    }
                }
            }

            if (count($item['children'])) {
                $res[$key]['children'] = $this->getChildrenMenu($item['children'], $lang, []);
            }
        }

        return $res;
    }

    public function getLinkByInterlinkData($inter_type, $id, $lang)
    {
        $url = '';

        if ($inter_type == Menu::INTER_TYPE_ARBITRARY) {
            return $id;
        } else {
            $model = null;
            switch ($inter_type) {
                case Menu::INTER_TYPE_PAGE:
                    $model = Pages::query()->where('id', $id)->active()->first();

                    if($model){
                        $url = \App\Models\Menu::getInterTypesModel()[$inter_type]['url_prefix'] . $model->path;
                    }
                    break;
                case Menu::INTER_TYPE_BLOG:
                    $model = BlogArticles::query()->where('id', $id)->active()->first();

                    if($model){
                        $cat = $model->tags[0]->slug;
                        $url = \App\Models\Menu::getInterTypesModel()[$inter_type]['url_prefix'] . $cat . '/' . $model->slug;
                    }
                    break;
            }

            if ($model) {
                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                    $url = '/' . $lang . '/' . $url;
                } else {
                    $url = '/' . $url;
                }
            } else {
                return '';
            }
        }

        return $url;
    }

    public function prepareFaqsResults($model, $lang): array
    {
        $data = [];

        foreach ($model as $key => $item) {
            $elem = [];

            $audio = AudioTranslation::query()
                ->where('audio_id', $item->audio_id)
                ->where('lang', $lang)
                ->first();

            $elem['audio_category'] = '';

            if (isset($audio->url)) {
                $a = Audio::query()
                    ->where('audio.id', $item->audio_id)
                    ->with('category', function ($q) use ($lang) {
                        $q->leftJoin('audio_category_translations', 'audio_category_translations.audio_categories_id', '=', 'audio_categories.id')
                            ->where('audio_category_translations.lang', $lang)
                            ->select(['audio_categories.*', 'audio_category_translations.title AS categoryName']);
                    })
                    ->first();

                if (isset($a['category']['categoryName'])) {
                    $elem['audio_category'] = $a['category']['categoryName'] ?? '';
                }
            }

            $elem['audio'] = $audio->url ?? null;
            $translate     = $item->getTranslation($lang);

            $elem['question'] = $translate->question;
            $elem['answer']   = $translate->answer;

            $data[] = $elem;
        }

        return $data;
    }

    private function getAnchorMenuFromDesc($translate)
    {
        $desc = $translate['description'];

        preg_match_all("/<[hH](10|[1-9]).*?>(.*?)<\/[hH](10|[1-9])>/", $desc, $items);

        $menu = [];

        if (count($items[0])) {
            foreach ($items[0] as $key => $item) {
                $id     = str_slug($items[2][$key], '_') . '_' . $key;
                $newH   = '<h' . $items[1][$key] . ' id="' . $id . '">' . $items[2][$key] . '</h' . $items[1][$key] . '>';

                $menu[] = [
                    'text' => $items[2][$key],
                    'id'   => $id
                ];

                $desc = str_replace($item,$newH,$desc);
            }
        }

        $translate['description'] = $desc;

       return $menu;
    }
}
