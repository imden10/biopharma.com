<?php


namespace App\Service;


use App\Models\BlogArticles;
use App\Models\Donors;
use App\Models\Menu;
use App\Models\Pages;

class MenuHelper
{
    const classes = [
        'pages' => 'App\Models\Pages',
        'blog'  => 'App\Models\BlogArticles'
    ];

    const names = [
        'pages' => 'Страница',
        'blog'  => 'Страница блога'
    ];

    const namesFromClass = [
        'App\Models\Pages' => 'Страница',
        'App\Models\BlogArticles'  => 'Страница блога'
    ];

    public function __renderSelectData()
    {
        $res = '<option value="">---</option>';

        $res .= '<optgroup label="Страницы">';
        foreach (Pages::query()->active()->get() as $item){
            $res .= '<option value="pages_'.$item->id.'">'.$item->title.'</option>';
        }
        $res .= '</optgroup>';


        $res .= '<optgroup label="Блог">';
        foreach (BlogArticles::query()->active()->get() as $item){
            $res .= '<option value="blog'.$item->id.'">'.$item->name.'</option>';
        }
        $res .= '</optgroup>';

        return $res;
    }

    public function renderSelectData($lang, $sel = null)
    {
        $res = '<option value="">---</option>';

        $isSelect = false;

        $res .= '<optgroup label="Страницы">';
        foreach (Pages::query()->active()->get() as $item){
            $selected = '';
            $url_prefix = Menu::getTypesModel()[Menu::TYPE_PAGE]['url_prefix'];

            $url = '/' . $url_prefix . $item->slug;

            if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                $url = '/' . $lang . '/' . $url_prefix . $item->slug;
            }

            if(! is_null($sel) && $sel === $url){
                $isSelect = true;
                $selected = 'selected';
            }

            $res .= '<option value="'.$url.'" '.$selected.'>'.$item->title.'</option>';
        }
        $res .= '</optgroup>';


        $res .= '<optgroup label="Доноры">';
        foreach (Donors::query()->active()->get() as $item){
            $selected = '';
            $url_prefix = Menu::getTypesModel()[Menu::TYPE_DONOR]['url_prefix'];

            $url = '/' . $url_prefix . $item->slug;

            if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                $url = '/' . $lang . '/' . $url_prefix . $item->slug;
            }

            if(! is_null($sel) && $sel === $url){
                $isSelect = true;
                $selected = 'selected';
            }

            $res .= '<option value="'.$url.'" '.$selected.'>'.$item->title.'</option>';
        }
        $res .= '</optgroup>';

        $selected = '';

        if(! is_null($sel) && ! $isSelect){
            $selected = 'selected';
        }

        $res .= '<optgroup label="Произвольная ссылка">';
        $res .= '<option value="0" '.$selected.'>Произвольная ссылка</option>';
        $res .= '</optgroup>';

        return $res;
    }
}
