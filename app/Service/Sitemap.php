<?php

namespace App\Service;

use App\Models\BlogArticles;
use App\Models\BlogTags;
use App\Models\Category;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Products;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Sitemap
{
    private $rows = 1;
    private $langs;
    private $defaultLangCode;

    public function __construct()
    {
        $this->langs = Langs::getLangCodes();
        $this->defaultLangCode = Langs::getDefaultLangCode();
    }

    public function generate()
    {
        echo "Start" . PHP_EOL;
        Log::info('sitemap generate start');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;


        $xml .= "<url>
                    <loc>".url('/')."</loc>
                    <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>1.0</priority>
                </url>";

        $list = Pages::query()->active()->get();
        foreach ($list as $item) {
            echo "Page: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/' . $item->slug, 'daily', '0.9',true, $item);
        }

        $list = BlogTags::query()->get();
        foreach ($list as $item) {
            echo "BlogTags: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/news/' . $item->slug, 'daily', '0.9',true, $item);
        }

        $list = BlogArticles::query()->active()->get();
        foreach ($list as $key => $item) {
            echo "BlogArticles: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/news/' . $item->tags[0]->slug . '/' . $item->slug, 'daily', '0.8', true, $item);
        }

        $list = Category::query()->active()->get();
        foreach ($list as $item) {
            echo "Category: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/products/' . $item->slug, 'daily', '0.8',true, $item);
        }

        $list = Products::query()->active()->get();
        foreach ($list as $key => $item) {
            echo "Products: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/products/' . $item->category->slug . '/' . $item->slug, 'daily', '0.8', true, $item);
        }

        $xml .= '</urlset>';

        file_put_contents(public_path('sitemap_file.xml'), $xml);
        chmod(public_path('sitemap_file.xml'), 0665);

        Log::info('sitemap generate end');

        return $this->rows;
    }

    private function isValidUri($uri, $isNews = false, $item = null, $lang = null)
    {
        if($isNews){
            return $item->hasLang($lang);
        }

        return true;

        //        $hds = @get_headers($uri);

        //        return !$hds || (strpos($hds[0], ' 404 ') !== false ) ? false : true;
    }

    public function row($loc, $changefreq, $priority, $isNews = false, $item = null)
    {
        $this->rows++;

        if($this->rows == 2){
            return '';
        }

        $url = url($loc);

        if(!$this->isValidUri($url,$isNews,$item, $this->defaultLangCode)) return '';

        $res = '';

        foreach ($this->langs as $lang){
            if($this->defaultLangCode == $lang){
                $locLang = $url;

                if($url == '//'){
                    $url = env('APP_URL');
                }

                if($this->isValidUri($locLang, $isNews, $item, $lang)){
                    $res .= '<url><loc>'.$url.'</loc>'.PHP_EOL;

                    if($item){
                        $res .= '<lastmod>'.$item->updated_at->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    } else {
                        $res .= '<lastmod>'.Carbon::now()->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    }

                    $res .= '<changefreq>'.$changefreq.'</changefreq><priority>'.$priority.'</priority></url>'.PHP_EOL;
                };
            }
        }

        foreach ($this->langs as $lang){
            if($this->defaultLangCode !== $lang){
                $locLang = url($lang . $loc);
                if($this->isValidUri($locLang, $isNews, $item, $lang)){
                    $res .= '<url><loc>'.$locLang.'</loc>'.PHP_EOL;

                    if($item){
                        $res .= '<lastmod>'.$item->updated_at->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    } else {
                        $res .= '<lastmod>'.Carbon::now()->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    }

                    $res .= '<changefreq>'.$changefreq.'</changefreq><priority>'.$priority.'</priority></url>'.PHP_EOL;
                }
            }
        }

        return $res;
    }
}
