<?php

namespace App\Console\Commands;

use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\PagesTranslation;
use App\Models\Translations\ProductTranslation;
use Illuminate\Console\Command;

class ParserMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserMeta:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse meta from self name and desc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        /* Pages */
        echo 'Pages' . PHP_EOL;
        $model = PagesTranslation::query()->get();
        $count = count($model);
        $suffDescription = [
            'uk' => " ➥ Фармацевтична компанія «Біофарма» в Україні ✔ Розробляємо й виробляємо ліки з плазми донорської крові людини! ",
            'ru' => " ➥ Фармацевтическая компания «Биофарма» в Украине ✔ Разрабатываем и производим лекарства из плазмы донорской крови человека!",
            "en" => ""
        ];

        if($count){
            foreach ($model as $j => $item){
                $item->meta_description =  $item->title . $suffDescription[$item->lang];
                $item->save();
                echo "save " . $j . '/' . $count . PHP_EOL;
            }
        }

        /* Products */
        echo 'Products' . PHP_EOL;
        $model = ProductTranslation::query()->get();
        $count = count($model);
        $suffDescription = [
            'uk' => " ✔ Міжнародний стандарт GMP ✔ Інноваційні препарати з плазми донорської крові людини ➥ Компанія «Біофарма» в Україні!",
            'ru' => " ✔ Международный стандарт GMP ✔ Инновационные препараты из плазмы донорской крови человека ➥ Компания «Биофарма» в Украине!",
            "en" => ""
        ];

        if($count){
            foreach ($model as $j => $item){
                $item->meta_description = $item->name . $suffDescription[$item->lang];
                $item->save();
                echo "save " . $j . '/' . $count . PHP_EOL;
            }
        }

        echo 'Blog' . PHP_EOL;
        $model = BlogArticleTranslation::query()->get();
        $count = count($model);
        $suffDescription = [
            'uk' => " ➥ Новини та статті фармацевтичної компанії «Біофарма» в Україні!",
            'ru' => " ➥ Новости и статьи фармацевтической компании «Биофарма» в Украине!",
            "en" => ""
        ];

        if($count){
            foreach ($model as $j => $item){
                $item->meta_description = $item->name . $suffDescription[$item->lang];
                $item->save();
                echo "save " . $j . '/' . $count . PHP_EOL;
            }
        }

        echo "ready";
    }
}
