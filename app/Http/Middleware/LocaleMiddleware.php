<?php

namespace App\Http\Middleware;

use App\Models\Langs;
use Closure;
use Illuminate\Support\Facades\App;

class LocaleMiddleware
{
  public $mainLanguage; //основной язык, который не должен отображаться в URl

  public $languages; // Указываем, какие языки будем использовать в приложении.

    public function __construct()
    {
        $this->mainLanguage = Langs::getDefaultLangCode();
        $this->languages = Langs::getLangCodes();
    }

    /*
     * Проверяет наличие корректной метки языка в текущем URL
     * Возвращает метку или значеие null, если нет метки
     */
  public function getLocale()
  {
    $uri = request()->path(); //получаем URI

    $segmentsURI = explode('/',$uri); //делим на части по разделителю "/"

    //Проверяем метку языка  - есть ли она среди доступных языков
    if (!empty($segmentsURI[0]) && in_array($segmentsURI[0], $this->languages)) {

      if ($segmentsURI[0] != $this->mainLanguage) return $segmentsURI[0];

    }
    return null;
  }

  /*
  * Устанавливает язык приложения в зависимости от метки языка из URL
  */
  public function handle($request, Closure $next)
  {
    $locale = $this->getLocale();
    /*if($locale) App::setLocale($locale);
    //если метки нет - устанавливаем основной язык $mainLanguage
    else App::setLocale(self::$mainLanguage);*/

    if($request->is('admin') || $request->is('admin/*') || !$locale){
        App::setLocale($this->mainLanguage);
    } else {
        App::setLocale($locale);
    }
    return $next($request); //пропускаем дальше - передаем в следующий посредник
  }

}
