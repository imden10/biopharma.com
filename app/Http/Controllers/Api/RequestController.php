<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Service\TelegramBot;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class RequestController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    private $bot;

    public function __construct(Adapter $adapter, TelegramBot $telegramBot)
    {
        $this->adapter = $adapter;
        $this->bot     = $telegramBot;
    }

    public function feedback(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['title'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['title']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $message = "Новый запрос: " . $decodedJson['title'] . PHP_EOL;

        if (isset($decodedJson['name'])) {
            $message .= "Имя - " . $decodedJson['name'] . PHP_EOL;
        }

        if (isset($decodedJson['company'])) {
            $message .= "Компания - " . $decodedJson['company'] . PHP_EOL;
        }

        if (isset($decodedJson['phone'])) {
            $message .= "Телефон - " . $decodedJson['phone'] . PHP_EOL;
        }

        if (isset($decodedJson['email'])) {
            $message .= "E-mail - " . $decodedJson['email'] . PHP_EOL;
        }

        if (isset($decodedJson['description'])) {
            $message .= "Вопрос - " . $decodedJson['description'] . PHP_EOL;
        }

        if (isset($decodedJson['data']) && isset($decodedJson['time'])) {
            $message .= "Дата и время - " . $decodedJson['data'] . ' ' . $decodedJson['time'] . PHP_EOL;
        }

        if (isset($decodedJson['with_someone'])) {
            $message .= "С кем вы хотите нас посетить - " . $decodedJson['with_someone'] . PHP_EOL;
        }

        $flag = $this->bot->sendMessage($message);

        return $this->successResponse([]);
    }
}
