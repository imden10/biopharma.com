<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\FormConsumer;
use App\Models\FormSpecialist;
use App\Service\TelegramBot;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class FormController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    private $bot;

    public function __construct(Adapter $adapter, TelegramBot $telegramBot)
    {
        $this->adapter = $adapter;
        $this->bot     = $telegramBot;
    }

    public function consumers(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }


        FormConsumer::create($decodedJson);

        $message = "Новий запит - <b>«Для споживачів»</b>" . PHP_EOL;


        $message .= PHP_EOL . "<b>Інформація про лікарський засіб</b>" . PHP_EOL;


        if (isset($decodedJson['prodName'])) {
            $message .= "Торгова назва - " . $decodedJson['prodName'] . PHP_EOL;
        }

        if (isset($decodedJson['ambulanceForm'])) {
            $message .= "Лікарська форма - " . $decodedJson['ambulanceForm'] . PHP_EOL;
        }

        if (isset($decodedJson['manufacturer'])) {
            $message .= "Виробник - " . $decodedJson['manufacturer'] . PHP_EOL;
        }


        $message .= PHP_EOL . "<b>Побічна реакція або відсутність ефективності</b>" . PHP_EOL;


        if (isset($decodedJson['PRDetail'])) {
            $message .= "Опис побічної реакції або вказівка про відсутність ефективності - " . $decodedJson['PRDetail'] . PHP_EOL;
        }


        $message .= PHP_EOL . "<b>Інформація про репортера</b>" . PHP_EOL;


        if (isset($decodedJson['userName'])) {
            $message .= "ПІБ повідомника - " . $decodedJson['userName'] . PHP_EOL;
        }

        if (isset($decodedJson['adres'])) {
            $message .= "Адреса - " . $decodedJson['adres'] . PHP_EOL;
        }

        if (isset($decodedJson['userEmail'])) {
            $message .= "E-mail - " . $decodedJson['userEmail'] . PHP_EOL;
        }

        if (isset($decodedJson['userPhone'])) {
            $message .= "Тел / факс - " . $decodedJson['userPhone'] . PHP_EOL;
        }


        $message .= PHP_EOL . "<b>Інформація про пацієнта</b>" . PHP_EOL;


        if (isset($decodedJson['pacientName'])) {
            $message .= "ПІБ пацієнта - " . $decodedJson['pacientName'] . PHP_EOL;
        }

        if (isset($decodedJson['age'])) {
            $message .= "Вік - " . $decodedJson['age'] . PHP_EOL;
        }

        if (isset($decodedJson['weight'])) {
            $message .= "Вага - " . $decodedJson['weight'] . PHP_EOL;
        }

        if (isset($decodedJson['diagnosis'])) {
            $message .= "Діагноз - " . $decodedJson['diagnosis'] . PHP_EOL;
        }

        if (isset($decodedJson['pacientAdres'])) {
            $message .= "Адреса - " . $decodedJson['pacientAdres'] . PHP_EOL;
        }

        if (isset($decodedJson['gender'])) {
            $message .= "Стать - " . $decodedJson['gender'] . PHP_EOL;
        }

        if (isset($decodedJson['pacientPhone'])) {
            $message .= "Тел / факс - " . $decodedJson['pacientPhone'] . PHP_EOL;
        }


        $message .= PHP_EOL . "<b>Інформація про призначення лікарського засобу</b>" . PHP_EOL;


        if (isset($decodedJson['doctorPrescribed'])) {
            $message .= "Лікарський засіб було призначено пацієнту лікарем - " . $decodedJson['doctorPrescribed'] . PHP_EOL;
        }

        if (isset($decodedJson['patientUsedDrugWithoutPrescription'])) {
            $message .= "Пацієнт застосував лікарський засіб без призначення лікаря - " . $decodedJson['patientUsedDrugWithoutPrescription'] . PHP_EOL;
        }


        $message .= PHP_EOL . "<b>Інформація про лікаря і про установу охорони здоров'я</b>" . PHP_EOL;


        if (isset($decodedJson['doctorName'])) {
            $message .= "ПІБ лікаря - " . $decodedJson['doctorName'] . PHP_EOL;
        }

        if (isset($decodedJson['healthCareInstitutionName'])) {
            $message .= "Назва установи охорони здоров'я - " . $decodedJson['healthCareInstitutionName'] . PHP_EOL;
        }

        if (isset($decodedJson['healthCareInstitutionAddress'])) {
            $message .= "Адреса установи охорони здоров'я - " . $decodedJson['healthCareInstitutionAddress'] . PHP_EOL;
        }

        if (isset($decodedJson['healthCareInstitutionPhone'])) {
            $message .= "Тел / факс - " . $decodedJson['healthCareInstitutionPhone'] . PHP_EOL;
        }

        $flag = $this->bot->sendMessage($message,true);

        return $this->successResponse([]);
    }

    public function specialists(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if($decodedJson['startDate']){
            $decodedJson['startDate'] = Carbon::parse($decodedJson['startDate']);
        }

        if(isset($decodedJson['endDate'])){
            $decodedJson['endDate'] = Carbon::parse($decodedJson['endDate']);
        }

        if($decodedJson['naslidkiExtra']){
            $decodedJson['naslidkiExtra'] = Carbon::parse($decodedJson['naslidkiExtra']);
        }

        if($decodedJson['terapiaStart']){
            $decodedJson['terapiaStart'] = Carbon::parse($decodedJson['terapiaStart']);
        }

        if($decodedJson['terapiaEnd']){
            $decodedJson['terapiaEnd'] = Carbon::parse($decodedJson['terapiaEnd']);
        }

        $prodItems = [];

        if($decodedJson['prodItems']){
            if(count($decodedJson['prodItems'])){
                $prodItems = $decodedJson['prodItems'];
                $decodedJson['prodItems'] = json_encode($decodedJson['prodItems']);
            } else {
                $decodedJson['prodItems'] = json_encode([]);
            }
        }

        $form = FormSpecialist::create($decodedJson);

        $message = "Новий запит - <b>«Для спціалістів»</b>" . PHP_EOL;

        /** ************************************** STEP 1 *************************************************************/

        $message .= PHP_EOL . "<b>Загальна інформація</b>" . PHP_EOL;

        if (isset($decodedJson['name'])) {
            $message .= "ПІБ пацієнта - " . $decodedJson['name'] . PHP_EOL;
        }

        if (isset($decodedJson['date'])) {
            $message .= "Дата народження / вік - " . $decodedJson['date'] . PHP_EOL;
        }

        if (isset($decodedJson['illNo'])) {
            $message .= "Номер історії хвороби/амбулаторної карти - " . $decodedJson['illNo'] . PHP_EOL;
        }

        if (isset($decodedJson['gender'])) {
            $message .= "Стать - " . $decodedJson['gender'] . PHP_EOL;
        }

        if (isset($decodedJson['weight'])) {
            $message .= "Вага (кг) - " . $decodedJson['weight'] . PHP_EOL;
        }

        if (isset($decodedJson['startDate'])) {
            $message .= "Початок ПР/ВЕ - " . $decodedJson['startDate']->format('d.m.Y') . ' ' . $decodedJson['startTime'] .  PHP_EOL;
        }

        if (isset($decodedJson['endDate'])) {
            $message .= "Завершення ПР - " . $decodedJson['endDate']->format('d.m.Y') . ' ' . $decodedJson['endTime'] .  PHP_EOL;
        }

        if (isset($decodedJson['description'])) {
            $message .= "Опис ПР/вказати ВЕ ЛЗ - " . $decodedJson['description'] . PHP_EOL;
        }

        if (isset($decodedJson['naslidki'])) {
            $message .= "Наслідки ПР/ВЕ - " . $decodedJson['naslidki'] . PHP_EOL;
        }

        if (isset($decodedJson['naslidkiExtra'])) {
            $message .= "Дата смерті - " . Carbon::parse($decodedJson['naslidkiExtra'])->format('d.m.Y') . PHP_EOL;
        }

        if (isset($decodedJson['categoriesPr'])) {
            $message .= "Категорія ПР/ВЕ - " . $decodedJson['categoriesPr'] . PHP_EOL;
        }

        if (isset($decodedJson['categoriesPrExtra'])) {
            $message .= "Дата смерті - " . Carbon::parse($decodedJson['categoriesPrExtra'])->format('d.m.Y') . PHP_EOL;
        }

        /** ***************************************** STEP 1 END ******************************************************/

        /** ***************************************** STEP 2 **********************************************************/

        $message .= PHP_EOL . "<b>Інформація про підозрюваний лікарський засіб (ПЛЗ)</b>" . PHP_EOL;

        if (isset($decodedJson['prodName'])) {
            $message .= "Торгова назва, лікарська форма - " . $decodedJson['prodName'] . PHP_EOL;
        }

        if (isset($decodedJson['manufacturer'])) {
            $message .= "Виробник - " . $decodedJson['manufacturer'] . PHP_EOL;
        }

        if (isset($decodedJson['prodNo'])) {
            $message .= "Номер серії - " . $decodedJson['prodNo'] . PHP_EOL;
        }

        if (isset($decodedJson['testimony'])) {
            $message .= "Показання (за можливості за МКХ-10) - " . $decodedJson['testimony'] . PHP_EOL;
        }

        if (isset($decodedJson['dose'])) {
            $message .= "Разова доза - " . $decodedJson['dose'] . PHP_EOL;
        }

        if (isset($decodedJson['multiplicity'])) {
            $message .= "Кратність застосування - " . $decodedJson['multiplicity'] . PHP_EOL;
        }

        if (isset($decodedJson['drugAdministration'])) {
            $message .= "Спосіб введення - " . $decodedJson['drugAdministration'] . PHP_EOL;
        }

        if (isset($decodedJson['terapiaStart']) && $decodedJson['terapiaStart']) {
            $message .= "Початок терапії ПЛЗ - " . $decodedJson['terapiaStart']->format('d.m.Y') . PHP_EOL;
        } elseif (isset($decodedJson['terapiaStartUndefined']) && $decodedJson['terapiaStartUndefined']) {
            $message .= "Початок терапії ПЛЗ - Не знаю" . PHP_EOL;
        }

        if (isset($decodedJson['terapiaEnd']) && $decodedJson['terapiaEnd']) {
            $message .= "Закінчення терапії ПЛЗ - " . $decodedJson['terapiaEnd']->format('d.m.Y') . PHP_EOL;
        } elseif (isset($decodedJson['terapiaEndUndefined']) && $decodedJson['terapiaEndUndefined']) {
            $message .= "Закінчення терапії ПЛЗ - Лікування триває" . PHP_EOL;
        }

        /** ***************************************** STEP 2 END ******************************************************/

        /** ***************************************** STEP 3 **********************************************************/

        $message .= PHP_EOL . "<b>Інформація про супутні лікарські засоби</b>" . PHP_EOL;

        if ($decodedJson['prodItems'] && $prodItems) {
            if (count($prodItems)) {
                foreach ($prodItems as $key => $prodItems){
                    $message .= "<b>Супутній лікарській засоб </b>" . ($key + 1) . PHP_EOL;

                    if (isset($decodedJson['prodName'])) {
                        $message .= "Торгова назва, лікарська форма - " . $decodedJson['prodName'] . PHP_EOL;
                    }

                    if (isset($decodedJson['manufacturer'])) {
                        $message .= "Виробник - " . $decodedJson['manufacturer'] . PHP_EOL;
                    }

                    if (isset($decodedJson['prodNo'])) {
                        $message .= "Номер серії - " . $decodedJson['prodNo'] . PHP_EOL;
                    }

                    if (isset($decodedJson['testimony'])) {
                        $message .= "Показання (за можливості за МКХ-10) - " . $decodedJson['testimony'] . PHP_EOL;
                    }

                    if (isset($decodedJson['dose'])) {
                        $message .= "Разова доза - " . $decodedJson['dose'] . PHP_EOL;
                    }

                    if (isset($decodedJson['multiplicity'])) {
                        $message .= "Кратність застосування - " . $decodedJson['multiplicity'] . PHP_EOL;
                    }

                    if (isset($decodedJson['drugAdministration'])) {
                        $message .= "Спосіб введення - " . $decodedJson['drugAdministration'] . PHP_EOL;
                    }

                    if (isset($decodedJson['terapiaStart']) && $decodedJson['terapiaStart']) {
                        $message .= "Початок терапії ПЛЗ - " . Carbon::parse($decodedJson['terapiaStart'])->format('d.m.Y') . PHP_EOL;
                    } elseif (isset($decodedJson['terapiaStartUndefined']) && $decodedJson['terapiaStartUndefined']) {
                        $message .= "Початок терапії ПЛЗ - Не знаю" . PHP_EOL;
                    }

                    if (isset($decodedJson['terapiaEnd']) && $decodedJson['terapiaEnd']) {
                        $message .= "Закінчення терапії ПЛЗ - " . Carbon::parse($decodedJson['terapiaEnd'])->format('d.m.Y') . PHP_EOL;
                    } elseif (isset($decodedJson['terapiaEndUndefined']) && $decodedJson['terapiaEndUndefined']) {
                        $message .= "Закінчення терапії ПЛЗ - Лікування триває" . PHP_EOL;
                    }
                }
            }
        }

        /** ***************************************** STEP 3 END ******************************************************/

        /** ***************************************** STEP 4 **********************************************************/

        $message .= PHP_EOL . "<b>Засоби корекції ПР</b>" . PHP_EOL;

        if (isset($decodedJson['PLZCancel'])) {
            $message .= "Скасування ПЛЗ - " . ($decodedJson['PLZCancel'] ? 'Так' : 'Ні') . PHP_EOL;
        }

        if (isset($decodedJson['PLZCancelingPR'])) {
            $message .= "Чи супроводжувалось скасування ПЛЗ припиненням ПР - " . $decodedJson['PLZCancelingPR'] . PHP_EOL;
        }

        if (isset($decodedJson['reassignmentPLZ'])) {
            $message .= "Повторне призначення ПЛЗ - " . ($decodedJson['reassignmentPLZ'] ? 'Так' : 'Ні') . PHP_EOL;
        }

        if (isset($decodedJson['PRRecoverAfterPLZ'])) {
            $message .= "Чи спостерігається відновлення ПР після повторного призначення ПЛЗ - " . $decodedJson['PRRecoverAfterPLZ'] . PHP_EOL;
        }

        if (isset($decodedJson['changingTheDoseRegimenOfPLZ'])) {
            $message .= "Зміна дозового режиму ПЛЗ - " . ($decodedJson['changingTheDoseRegimenOfPLZ'] ? 'Так' : 'Ні') . PHP_EOL;
        }

        if (isset($decodedJson['changingTheDoseRegimenOfPLZtext'])) {
            $message .= "Зниження/підвищення, зазначити на скільки - " . $decodedJson['changingTheDoseRegimenOfPLZtext'] . PHP_EOL;
        }

        if (isset($decodedJson['PR_VE'])) {
            $message .= "ПР/ВЕ - " . $decodedJson['PR_VE'] . PHP_EOL;
        }

        if (isset($decodedJson['additionInformation'])) {
            $message .= "Вказати ЛЗ, дозовий режим, тривалість призначення - " . $decodedJson['additionInformation'] . PHP_EOL;
        }

        /** ***************************************** STEP 4 END ******************************************************/

        /** ***************************************** STEP 5 **********************************************************/

        $message .= PHP_EOL . "<b>Інформація про повідомника</b>" . PHP_EOL;

        if (isset($decodedJson['informerName'])) {
            $message .= "ПІБ повідомника - " . $decodedJson['informerName'] . PHP_EOL;
        }

        if (isset($decodedJson['informerPhoneFax'])) {
            $message .= "Тел / факс - " . $decodedJson['informerPhoneFax'] . PHP_EOL;
        }

        if (isset($decodedJson['informerEmail'])) {
            $message .= "E-mail - " . $decodedJson['informerEmail'] . PHP_EOL;
        }

        if (isset($decodedJson['informerClinicName'])) {
            $message .= "Назва та місцезнаходження закладу охорони здоров'я або заявника - " . $decodedJson['informerClinicName'] . PHP_EOL;
        }

        if (isset($decodedJson['informerReporter'])) {
            $message .= "Репортер - " . $decodedJson['informerReporter'] . PHP_EOL;
        }

        /** ***************************************** STEP 5 END ******************************************************/

        $flag = $this->bot->sendMessage($message,true);

        return $this->successResponse([]);
    }
}
