<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Settings;
use App\Service\Adapter;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getAll(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['lang'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['lang']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Settings::query()
            ->select([
                'code', 'value', 'lang'
            ])
            ->get()
            ->groupBy('lang')->transform(function($item, $k) {
                return $item->groupBy('code');
            })
            ->toArray();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $settings = $model[$lang];


        if(Langs::getDefaultLangCode() !== $lang){
            $settingKeys = array_keys($settings);
            $settingDefaultKeys = array_keys($model[Langs::getDefaultLangCode()]);
            $diffKeys = array_diff($settingDefaultKeys,$settingKeys);

            if(count($diffKeys)){
                foreach ($model[Langs::getDefaultLangCode()] as $key => $val){
                    if(in_array($key,$diffKeys)){
                        $settings[$key] = $val;
                    }
                }
            }
        }

        $res = [];

        foreach ($settings as $key => $item){
            if($this->isJSON($item[0]['value'])){
                $val = array_values(json_decode($item[0]['value'],true));
            } else {
                $val = $item[0]['value'];

                if(in_array($key,['donors_menu','plasmacenters_menu','footer_menu'])){
                    $main = \App\Models\Menu::query()
                        ->where('id',$val)
                        ->where('const',  1)
                        ->first();

                    if(! $main){
                        continue;
                    }

                    $modelM = \App\Models\Menu::query()
                        ->where('tag', $main->tag)
                        ->where('const', '<>', 1)
                        ->with(['page','blog','landing','donor','plasmacentr'])
                        ->defaultOrder()
                        ->get()
                        ->toTree()
                        ->toArray();

                    $val = $this->adapter->prepareMenuResults($modelM,$lang);
                }
            }

            $res[$key] = $val;
        }

        return $this->successResponse($res);
    }

    private function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }
}
