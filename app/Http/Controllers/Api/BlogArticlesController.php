<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\BlogArticleTag;
use App\Models\BlogTags;
use App\Models\Langs;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class BlogArticlesController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = BlogArticles::query()
            ->leftJoin('blog_article_tag', 'blog_article_tag.blog_article_id', '=', 'blog_articles.id')
            ->leftJoin('blog_tags', 'blog_tags.id', '=', 'blog_article_tag.blog_tag_id')
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_article_tag.blog_tag_id')
            ->where('blog_tag_translations.lang', $lang)
            ->select([
                'blog_articles.*',
                'blog_article_tag.blog_tag_id AS category_id',
                'blog_tags.slug AS category_slug',
                'blog_tag_translations.name AS category_name',
            ])
            ->active()
            ->where('blog_articles.slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $data = $this->adapter->prepareModelResults($model, $lang);

        $model->views = ($model->views + 1);
        $model->save();

        if (isset($model->tags[0]->id)) {
            $similarIds = BlogArticleTag::query()->where('blog_tag_id', $model->tags[0]->id)->pluck('blog_article_id')->toArray();

            $similarModels = $model = BlogArticles::query()
                ->active()
                ->whereIn('id', $similarIds)
                ->where('id', '<>', $model->id)
                ->orderBy('public_date', 'desc')
                ->limit(3)
                ->get();

            $similarData = [];

            $getDefaultLangCode = \App\Models\Langs::getDefaultLangCode();

            foreach ($similarModels as $item) {
                $translate = $item->getTranslation($lang);

                $cat = $item->tags[0]->slug ?? null;

                $url = '/news/' . ($cat ? ($cat . '/') : '') . $item->slug;

                if ($lang !== $getDefaultLangCode) {
                    $url = '/' . $lang . $url;
                }

                $similarData[] = [
                    'name'  => $translate['name'],
                    'text'  => $translate['text'],
                    'slug'  => $item->slug,
                    'url'   => $url,
                    'date'  => Carbon::parse($item->public_date)->format('d.m.Y'),
                    'image' => $item->image
                ];
            }

            $data['similar_posts'] = $similarData;
        }

        return $this->successResponse($data);
    }

    public function getAll(Request $request)
    {
        $take = (int)app(Setting::class)->get('news_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $model = BlogArticles::query()
            ->leftJoin('blog_article_tag', 'blog_article_tag.blog_article_id', '=', 'blog_articles.id')
            ->leftJoin('blog_tags', 'blog_tags.id', '=', 'blog_article_tag.blog_tag_id')
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_article_tag.blog_tag_id')
            ->where('blog_tag_translations.lang', $lang)
            ->select([
                'blog_articles.*',
                'blog_article_tag.blog_tag_id AS category_id',
                'blog_tags.slug AS category_slug',
                'blog_tag_translations.name AS category_name',
            ])
            ->orderBy('blog_articles.created_at', 'desc')
            ->active()
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareModelsResults($model, $lang, 150);

        $paginate = [
            'total'        => BlogArticles::query()->active()->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $categories = BlogTags::query()
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_tags.id')
            ->where('blog_tag_translations.lang', $lang)
            ->whereHas('articles')
            ->select([
                'blog_tags.*',
                'blog_tag_translations.name AS catName'
            ])
            ->get();

        $categoriesArr = [];

        if (count($categories)) {
            $getDefaultLangCode = \App\Models\Langs::getDefaultLangCode();

            foreach ($categories as $item) {
                $url = '/news/' . $item->slug;

                if ($lang !== $getDefaultLangCode) {
                    $url = '/' . $lang . $url;
                }

                $categoriesArr[] = [
                    'title' => $item->catName,
                    'slug'  => $item->slug,
                    'url'   => $url
                ];
            }
        }

        $data['categories'] = $categoriesArr;

        try {
            $widgetSeeAlso = Widget::query()->where('id', 58)->where('lang', $lang)->first()->toArray();

            if ($widgetSeeAlso) {
                $seeAlso = [];
                foreach ($widgetSeeAlso['data']['list'] as $item) {
                    $seeAlso[] = [
                        'title'          => $item['title'],
                        'interlink_type' => $item['interlink_type'],
                        'url'            => $this->adapter->getLinkByInterlinkData($item['interlink_type'], $item['interlink_val'][$item['interlink_type']], $lang)
                    ];
                }

                $data['see_also'] = [
                    'title' => $widgetSeeAlso['data']['title'],
                    'list' => $seeAlso
                ];
            }
        } catch (\Throwable $e){

        }

        return $this->successResponse($data);
    }

    public function getByCategory(Request $request)
    {
        $take = (int)app(Setting::class)->get('news_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $category = BlogTags::query()
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_tags.id')
            ->where('blog_tag_translations.lang', $lang)
            ->where('blog_tags.slug', $decodedJson['slug'])
            ->select([
                'blog_tags.*',
                'blog_tag_translations.name AS catName'
            ])
            ->first();

        if (!$category)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $currentPage = $decodedJson['page'] ?? 1;

        $catIds = BlogArticleTag::query()->where('blog_tag_id', $category->id)->pluck('blog_article_id')->toArray();

        $model = BlogArticles::query()
            ->leftJoin('blog_article_tag', 'blog_article_tag.blog_article_id', '=', 'blog_articles.id')
            ->leftJoin('blog_tags', 'blog_tags.id', '=', 'blog_article_tag.blog_tag_id')
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_article_tag.blog_tag_id')
            ->where('blog_tag_translations.lang', $lang)
            ->select([
                'blog_articles.*',
                'blog_article_tag.blog_tag_id AS category_id',
                'blog_tags.slug AS category_slug',
                'blog_tag_translations.name AS category_name',
            ])
            ->whereIn('blog_articles.id', $catIds)
            ->orderBy('blog_articles.created_at', 'desc')
            ->active()
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareModelsResults($model, $lang, 150);

        $paginate = [
            'total'        => BlogArticles::query()->active()->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $categories = BlogTags::query()
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_tags.id')
            ->where('blog_tag_translations.lang', $lang)
            ->whereHas('articles')
            ->select([
                'blog_tags.*',
                'blog_tag_translations.name AS catName'
            ])
            ->get();

        $categoriesArr = [];

        if (count($categories)) {
            $getDefaultLangCode = \App\Models\Langs::getDefaultLangCode();

            foreach ($categories as $item) {
                $url = '/news/' . $item->slug;

                if ($lang !== $getDefaultLangCode) {
                    $url = '/' . $lang . $url;
                }

                $categoriesArr[] = [
                    'title' => $item->catName,
                    'slug'  => $item->slug,
                    'url'   => $url
                ];
            }
        }

        $data['categories'] = $categoriesArr;

        try {
            $widgetSeeAlso = Widget::query()->where('id', 58)->where('lang', $lang)->first()->toArray();

            if ($widgetSeeAlso) {
                $seeAlso = [];
                foreach ($widgetSeeAlso['data']['list'] as $item) {
                    $seeAlso[] = [
                        'title'          => $item['title'],
                        'interlink_type' => $item['interlink_type'],
                        'url'            => $this->adapter->getLinkByInterlinkData($item['interlink_type'], $item['interlink_val'][$item['interlink_type']], $lang)
                    ];
                }

                $data['see_also'] = [
                    'title' => $widgetSeeAlso['data']['title'],
                    'list' => $seeAlso
                ];
            }
        } catch (\Throwable $e){

        }

        return $this->successResponse($data);
    }

    public function search(Request $request)
    {
        $take = (int)app(Setting::class)->get('news_per_page', 'uk');

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery = BlogArticles::query()
            ->leftJoin('blog_article_tag', 'blog_article_tag.blog_article_id', '=', 'blog_articles.id')
            ->leftJoin('blog_tags', 'blog_tags.id', '=', 'blog_article_tag.blog_tag_id')
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_article_tag.blog_tag_id')
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_tag_translations.lang', $lang)
            ->where('blog_article_translations.lang', $lang)
            ->select([
                'blog_articles.*',
                'blog_article_tag.blog_tag_id AS category_id',
                'blog_tags.slug AS category_slug',
                'blog_tag_translations.name AS category_name',
            ])
            ->orderBy('blog_articles.public_date', 'desc')
            ->where('blog_articles.status',BlogArticles::STATUS_ACTIVE)
            ->where(function($q) use($decodedJson){
                if($decodedJson['q']){
                    $q->where('blog_article_translations.name','like','%' . $decodedJson['q'] . '%')
                        ->orWhere('blog_article_translations.text','like','%' . $decodedJson['q'] . '%');
                }


            });


        $total = $articlesQuery->count();

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'items' => $items
        ];

        $categories = BlogTags::query()
            ->leftJoin('blog_tag_translations', 'blog_tag_translations.blog_tags_id', '=', 'blog_tags.id')
            ->where('blog_tag_translations.lang', $lang)
            ->select([
                'blog_tags.*',
                'blog_tag_translations.name AS catName'
            ])
            ->get();

        $categoriesArr = [];

        if (count($categories)) {
            $getDefaultLangCode = \App\Models\Langs::getDefaultLangCode();

            foreach ($categories as $item) {
                $url = '/news/' . $item->slug;

                if ($lang !== $getDefaultLangCode) {
                    $url = '/' . $lang . $url;
                }

                $categoriesArr[] = [
                    'title' => $item->catName,
                    'slug'  => $item->slug,
                    'url'   => $url
                ];
            }
        }

        $data['categories'] = $categoriesArr;

        try {
            $widgetSeeAlso = Widget::query()->where('id', 58)->where('lang', $lang)->first()->toArray();

            if ($widgetSeeAlso) {
                $seeAlso = [];
                foreach ($widgetSeeAlso['data']['list'] as $item) {
                    $seeAlso[] = [
                        'title'          => $item['title'],
                        'interlink_type' => $item['interlink_type'],
                        'url'            => $this->adapter->getLinkByInterlinkData($item['interlink_type'], $item['interlink_val'][$item['interlink_type']], $lang)
                    ];
                }

                $data['see_also'] = [
                    'title' => $widgetSeeAlso['data']['title'],
                    'list' => $seeAlso
                ];
            }
        } catch (\Throwable $e){

        }

        return $this->successResponse($data);
    }
}
