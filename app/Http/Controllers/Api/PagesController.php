<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class PagesController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Pages::query()
            ->active()
            ->where('slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $data = $this->adapter->prepareModelResults($model,$lang);

        if($decodedJson['slug'] === 'faq'){
            $faqModel = Faq::query()->get();
            $faqData = $this->adapter->prepareFaqsResults($faqModel,$lang);

            $widgetQuestionsForm = Widget::query()->where('instance','questions-form')->where('lang',$lang)->first()->toArray();

            if($widgetQuestionsForm){
                $data['constructor'] = [
                    'questions-form' => isset($widgetQuestionsForm) ? $widgetQuestionsForm['data'] : [],
                ];
            }

            $data['faq_data'] = $faqData;
        }

        return $this->successResponse($data);
    }
}
