<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Langs;
use App\Models\Products;
use App\Modules\Setting\Setting;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class ProductsController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Products::query()
            ->leftJoin('category', 'category.id', '=', 'products.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'products.category_id')
            ->where('category_translations.lang', $lang)
            ->select([
                'products.*',
                'category.slug AS categorySlug',
                'category_translations.title AS categoryName'
            ])
            ->active()
            ->where('products.slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $data = $this->adapter->prepareProductResults($model, $lang);

        $similar = $model->getSimilarModels($lang);

        $data['similar'] = $this->adapter->prepareProductsResults($similar, $lang);

        return $this->successResponse($data);
    }

    public function getAll(Request $request)
    {
        $take = (int)app(Setting::class)->get('products_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $model = Products::query()
            ->leftJoin('category', 'category.id', '=', 'products.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'products.category_id')
            ->where('category_translations.lang', $lang)
            ->select([
                'products.*',
                'category.slug AS categorySlug',
                'category_translations.title AS categoryName'
            ])
            ->orderBy('products.order', 'asc')
            ->active()
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareProductsResults($model, $lang);

        $paginate = [
            'total'        => Products::query()->active()->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $data['categories'] = $this->getAllCategories($lang);

        return $this->successResponse($data);
    }

    public function getByCategory(Request $request)
    {
        $take = (int)app(Setting::class)->get('products_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $category = Category::query()
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
            ->where('category_translations.lang', $lang)
            ->where('category.slug', $decodedJson['slug'])
            ->select([
                'category.*',
                'category_translations.title AS catName',
                'category_translations.h1 AS catH1',
            ])
            ->first();

        if (!$category)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $currentPage = $decodedJson['page'] ?? 1;

        $model = Products::query()
            ->leftJoin('category', 'category.id', '=', 'products.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'products.category_id')
            ->where('category_translations.lang', $lang)
            ->select([
                'products.*',
                'category.slug AS categorySlug',
                'category_translations.title AS categoryName'
            ])
            ->where('products.category_id', $category->id)
            ->orderBy('products.order', 'asc')
            ->active()
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareProductsResults($model, $lang);

        $paginate = [
            'total'        => Products::query()->active()->where('category_id', $category->id)->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $data['category'] = [
            'name' => $category->catName,
            'h1'   => $category->catH1
        ];

        $data['categories'] = $this->getAllCategories($lang);

        return $this->successResponse($data);
    }

    private function getAllCategories($lang)
    {
        $categories = Category::query()
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
            ->where('category_translations.lang', $lang)
            ->select([
                'category.*',
                'category_translations.title AS catName',
                'category_translations.h1 AS catH1',
            ])
            ->get();

        $categoriesArr = [];

        if (count($categories)) {
            $getDefaultLangCode = \App\Models\Langs::getDefaultLangCode();

            foreach ($categories as $item) {
                $url = '/preparati/' . $item->slug;

                if ($lang !== $getDefaultLangCode) {
                    $url = '/' . $lang . $url;
                }

                $categoriesArr[] = [
                    'title' => $item->catName,
                    'h1'    => $item->catH1,
                    'slug'  => $item->slug,
                    'url'   => $url
                ];
            }
        }

        return $categoriesArr;
    }
}
