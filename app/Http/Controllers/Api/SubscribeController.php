<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Settings;
use App\Models\Subscribes;
use App\Service\Adapter;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class SubscribeController extends Controller
{
    use ResponseTrait;

    public function subscribe(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['email'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['email']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if(! filter_var($decodedJson['email'], FILTER_VALIDATE_EMAIL)){
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_EMAIL_NOT_VALID, ['email']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (Subscribes::query()->where('email',$decodedJson['email'])->exists()) {
            return $this->errorResponse(['Email already exists']);
        }

        try {
            Subscribes::create(['email' => $decodedJson['email']]);
        } catch (\Throwable $e){
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_EXCEPTION, [$e->getMessage()]),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return $this->successResponse([]);
    }
}
