<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Langs;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $name = $request->get('name');

        $model = Category::query()
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
            ->where('category_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'category.*',
                'category_translations.title'
            ])
            ->orderBy('category.order', 'asc')
            ->where(function ($q) use ($status,$name) {
                if ($status != '') {
                    $q->where('category.status', $status);
                }
                if ($name != '') {
                    $q->where('category_translations.title', 'like', '%' . $name . '%')
                        ->orWhere('category_translations.title', 'like', '%' . $name . '%');
                }
            })
            ->paginate(20);

        return view('admin.category.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Category();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        $categories = $model->treeStructure();

        return view('admin.category.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Category();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Category::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->status = $request->get('status') ?? false;

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $model->translateOrNew($lang)->fill($data);

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('category.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('category.index')->with('success', 'Категория успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Category::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        $categories = $model->treeStructure();

        return view('admin.category.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model Category */
        $model = Category::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->status = $request->get('status') ?? false;

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $model->translateOrNew($lang)->fill($data);

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('category.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Категория успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->deleteTranslations();
        $category->delete();

        return redirect()->back()->with('success', 'Категорию успешно удалено!');
    }
}
