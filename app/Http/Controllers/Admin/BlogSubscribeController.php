<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscribes;
use Illuminate\Http\Request;

class BlogSubscribeController extends Controller
{
    public function index()
    {
        $model = Subscribes::query()
            ->orderBy('created_at','desc')
            ->paginate(50);

        return view('admin.blog.subscribe.index',[
            'model' => $model
        ]);
    }

    public function list()
    {
        $model = Subscribes::query()
            ->orderBy('created_at','desc')
            ->get();

        $data = "";

        foreach ($model as $key => $item){
            $data .= $item->email;

            if($key < count($model) - 1){
                $data .= PHP_EOL;
            }
        }

        return view('admin.blog.subscribe.list',[
            'data' => $data
        ]);
    }

    public function delete(Request $request)
    {
        Subscribes::query()->where('id',$request->get('id'))->delete();

        return redirect()->back()->with('success','Запись удалена!');
    }
}
