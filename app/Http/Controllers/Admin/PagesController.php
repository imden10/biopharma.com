<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $title = $request->get('title');

        $model = Pages::query()
            ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
            ->where('pages_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'pages.*',
                'pages_translations.title'
            ])
            ->orderBy('pages.status', 'desc')
            ->orderBy('pages.created_at', 'desc')
            ->where(function ($q) use ($status,$title) {
                if ($status != '') {
                    $q->where('pages.status', $status);
                }
                if ($title != '') {
                    $q->where('pages_translations.title', 'like', '%' . $title . '%');
                }
            })
            ->paginate(20);

        return view('admin.pages.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Pages();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.pages.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param PagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Pages();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Pages::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $this->setPath($model);

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('pages.index')->with('success', 'Страница успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pages $page)
    {
        $localizations = Langs::getLangsWithTitle();

//        $menu = PagesMenu::query()
//            ->where('page_id', $page->id)
//            ->orderBy('order', 'asc')
//            ->get();


        return view('admin.pages.edit', [
            'model'         => $page,
            'data'          => $page->getTranslationsArray(),
            'localizations' => $localizations
//            'menu'          => $menu
        ]);
    }

    /**
     * @param PagesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Pages */
        $model = Pages::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $this->setPath($model);

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Страница успешно обновлена!');
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Pages $page)
    {
        $page->deleteTranslations();
        $page->delete();

        return redirect()->back()->with('success', 'Страницу успешно удалено!');
    }

    /* Generate category path */
    public function setPath($model)
    {
        try {
            $path = '';
            $parent = 0;

            do {
                if (!$parent) {
                    $tmp_slug = $model->slug;
                    $parent = $model->parent_id;
                } else {
                    $parent_model = app(Pages::class)->find((int)$parent);
                    $tmp_slug = $parent_model->slug;
                    $parent = $parent_model->parent_id;
                }
                if ($path) {
                    $path = $tmp_slug . '/' . $path;
                } else{
                    $path = $tmp_slug;
                }
            } while ($parent != 0);
            //dd($path);

            $model->path = $path;
            $model->save();

            $children = app(Pages::class)->where('parent_id', '=', (int) $model->id)->pluck('id')->toArray();
            if($children){
                //dd($children);
                foreach ($children as $child){
                    $this->setPath((int)$child);
                }
            }
            return $path;
        }
        catch (\Exception $e) {
            //echo $e;
            return null;
        }
    }
}
