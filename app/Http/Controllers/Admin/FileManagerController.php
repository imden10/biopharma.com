<?php

namespace App\Http\Controllers\Admin;

use App\Handlers\Events\AuthLoginEventHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccessGroupCreateRequest;
use App\Models\AccessGroupPermissions;
use App\Models\AccessGroups;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FileManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function file()
    {
        return view('admin.file-manager.file', [

        ]);
    }

    public function image()
    {
        return view('admin.file-manager.image', [

        ]);
    }
}
