<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\ModelExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportTableController extends Controller
{
    public function export(Request $request)
    {
        $params = [];

        $filter = [];

        if($request->has('params')){
            try {
                $params = json_decode($request->get('params'),true);

                if(isset($params['date'])){
                    $dateFrom = substr($params['date'],0,10);
                    $dateTo = substr($params['date'],13,10);

                    $filter['dateFrom'] = $dateFrom;
                    $filter['dateFrom'] = Carbon::parse($filter['dateFrom'])->format('Y-m-d') . ' 00:00:00';
                    $filter['dateTo'] = $dateTo;
                    $filter['dateTo'] = Carbon::parse($filter['dateTo'])->format('Y-m-d') . ' 23:59:59';}

                if(isset($params['promo'])){
                    $filter['promo'] = $params['promo'];
                }

            } catch (\Exception $e){

            }
        }

        $model = DB::table($request->get('table'))
            ->where(function ($q)use($filter){
                if(count($filter)){
                    if(isset($filter['dateFrom']) && isset($filter['dateTo'])){
                        $q->whereBetween('created_at', [$filter['dateFrom'], $filter['dateTo']]);
                    }

                    if(isset($filter['promo'])){
                        $q->where('promo', $filter['promo']);
                    }
                }
            })
            ->select($request->get('fields'))
            ->get();

        return Excel::download(new ModelExport($model,$request->get('fields')), $request->get('table') . '.' . $request->get('aggregator'));
    }
}
