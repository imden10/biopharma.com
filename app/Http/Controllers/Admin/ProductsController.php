<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Langs;
use App\Models\ProductAttributes;
use App\Models\ProductPrices;
use App\Models\Products;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller
{
    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $name = $request->get('name');
        $category = $request->get('category');

        $model = Products::query()
            ->leftJoin('product_translations', 'product_translations.products_id', '=', 'products.id')
            ->where('product_translations.lang',Langs::getDefaultLangCode())
            ->select([
              'products.*',
              'product_translations.name'
            ])
            ->orderBy('products.created_at', 'desc')
            ->where(function ($q) use ($status,$name,$category) {
                if ($status != '') {
                    $q->where('products.status', $status);
                }
                if ($name != '') {
                    $q->where('product_translations.name', 'like', '%' . $name . '%');
                }
                if ($category != '') {
                    $q->where('products.category_id',$category);
                }
            })
            ->paginate(50);

        return view('admin.products.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Products();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        $categories = $this->category->treeStructure();

        return view('admin.products.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Products();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Products::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $model->status = $request->get('status') ?? false;

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []),$constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('products.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('products.edit', $model->id)->with('success', 'Продукт успешно добавлен!');
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Products::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        $categories = $this->category->treeStructure();

        return view('admin.products.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Products */
        $model = Products::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->status = $request->get('status') ?? false;

            $model->fill($request->all());

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->similar()->sync($request->get('similar_products'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');

                $data = $request->input('page_data.' . $lang, []);

                if(isset($data['package']) && count($data['package'])){
                    $data['package'] = json_encode($data['package'],JSON_UNESCAPED_UNICODE);
                } else {
                    $data['package'] = json_encode([]);
                }

                if(isset($data['indication']) && count($data['indication'])){
                    $data['indication'] = json_encode($data['indication'],JSON_UNESCAPED_UNICODE);
                } else {
                    $data['indication'] = json_encode([]);
                }

                if(isset($data['catalogIndication']) && count($data['catalogIndication'])){
                    $data['catalogIndication'] = json_encode($data['catalogIndication'],JSON_UNESCAPED_UNICODE);
                } else {
                    $data['catalogIndication'] = json_encode([]);
                }

                if(isset($data['contraindications']) && count($data['contraindications'])){
                    $data['contraindications'] = json_encode($data['contraindications'],JSON_UNESCAPED_UNICODE);
                } else {
                    $data['contraindications'] = json_encode([]);
                }

                if(isset($data['publication']) && count($data['publication'])){
                    foreach ($data['publication'] as $key => $publication){
                        $parts = explode('/files/',$publication['file']);
                        if(isset($parts[1])){
                            $data['publication'][$key]['file'] = '/' . $parts[1];
                        }
                    }

                    $data['publication'] = json_encode($data['publication'],JSON_UNESCAPED_UNICODE);
                } else {
                    $data['publication'] = json_encode([]);
                }

                $model->translateOrNew($lang)->fill(array_merge($data,$constructorData[$lang] ?? []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('products.edit', $model->id)->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Продукт успешно обновлен!');
    }

    /**
     * @param Products $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Products $product)
    {
        $product->deleteTranslations();
        $product->similar()->sync([]);

        $product->delete();

        return redirect()->back()->with('success', 'Продукт успешно удалено!');
    }
}
