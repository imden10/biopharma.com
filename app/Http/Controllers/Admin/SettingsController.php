<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Donors;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Plasmacenters;
use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index($tab)
    {
        $model = Settings::query()
            ->select([
                'code', 'value', 'lang'
            ])
            ->get()
            ->groupBy('lang')->transform(function($item, $k) {
                return $item->groupBy('code');
            })
            ->toArray();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.settings.index', [
            'data'          => $model,
            'tab'           => $tab,
            'localizations' => $localizations
        ]);
    }

    public function save(Request $request)
    {
        $post = $request->except(['_token']);

        foreach ($post['setting_data'] as $lang => $data){
            foreach ($data as $code => $value) {
                $item = Settings::firstOrNew([
                    'code' => $code,
                    'lang' => $lang
                ]);

                $newVal = $value;

                if (is_array($value)) {
                    $newVal = json_encode($value, JSON_UNESCAPED_UNICODE);
                }

                $item->value = $newVal;
                $item->save();

                if($lang == Langs::getDefaultLangCode()){

                }
            }
        }

        return redirect()->back()->with('success', 'Настройки успешно сохранены!');
    }
}
