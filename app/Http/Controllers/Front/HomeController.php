<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if ( $_SERVER['REQUEST_URI'] != strtolower( $_SERVER['REQUEST_URI']) ) {
            header('Location: http://'.$_SERVER['HTTP_HOST'] .
                strtolower($_SERVER['REQUEST_URI']), true, 301);
            exit();
        }

        return view('front.home.frontend',[]);
    }

    public function sitemap()
    {
        $pathToFile = public_path() . '/sitemap_file.xml';

        return response()->file($pathToFile,['Content-Type' => 'text/xml']);
    }
}
