<?php
    $interlinkSel = old($field['name'], $value);
    if(isset($interlinkSel) && is_array($interlinkSel) && isset($interlinkSel['interlink_type']) && isset($interlinkSel['interlink_val'])){
        $interlinkSelType = $interlinkSel['interlink_type'];
        $interlinkSelVal = $interlinkSel['interlink_val'];
    }
?>

@include('admin.widget-fields.interlink.interlink',[
    'label' =>  trans($field['label']),
    'name_type' => $field['name'] .'[interlink_type]',
    'name_val' => $field['name'] .'[interlink_val]',
    'sel_type' => $interlinkSelType ?? null,
    'sel_val' => $interlinkSelVal ?? null,
])

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $('.items-container').find('.select2-field').each(function () {
            $(this).select2({});
        });
    </script>
@endpush
