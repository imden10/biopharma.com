<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['title'] }}</label>

    <input type="text" name="{{ $field['name'] }}[name_text]" placeholder="{{ $field['label_text'] }}" value="{{$value['name_text'] ?? ''}}"  class="form-control mb-1">
    <?php $sel = $value['name_url'] ?? '' ?>
    <select class="select2-field menu-url-select" style="width: 100%">
        {!! app(\App\Service\MenuHelper::class)->renderSelectData(request()->get('lang'),$sel) !!}
    </select>
    <input type="text" name="{{ $field['name'] }}[name_url]" style="margin-top: 20px" value="{{$sel}}" placeholder="{{ $field['label_url'] }}" class="form-control mb-1 disabled menu-url-input">
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
    <style>
        input.disabled {
            pointer-events: none;
            background-color: #e9ecef;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           $('.select2-field').each(function () {
                $(this).select2({});

               let val = $(this).find("option:selected").val();

               if(val === '0'){
                   $(this).siblings(".menu-url-input").removeClass('disabled');
               }
            });
           
           $(document).on('change',".menu-url-select",function () {
               let val = $(this).find("option:selected").val();
               if(val !== '0'){
                   $(this).siblings(".menu-url-input").val(val);
                   $(this).siblings(".menu-url-input").addClass('disabled');
               } else {
                   $(this).siblings(".menu-url-input").val('');
                   $(this).siblings(".menu-url-input").removeClass('disabled');
               }
           })
        });
    </script>
@endpush
