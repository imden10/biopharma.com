<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <?php
                    $list = \App\Models\Faq::query()->pluck('question','id')->toArray();
                ?>
                <select class="select2-elem" name="{{ $field['name'] }}[#dynamicListPlaceholder][faq_id]">
                    @if(count($list))
                        @foreach($list as $key => $text)
                            <option value="{{$key}}">{{$text}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                    <select class="select2-elem" name="{{ $field['name'] }}[{{ $key }}][faq_id]">
                        @if(count($list))
                            @foreach($list as $id => $text)
                                <option value="{{$id}}" @if($value['faq_id'] == $id) selected @endif>{{$text}}</option>
                            @endforeach
                        @endif
                    </select>

                    <div class="col-lg-2">
                        <button type="button" class="btn btn-danger remove-item float-right">Видалити</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Додати</button>
</div>

@push('styles')
{{--    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">--}}
@endpush

@push('scripts')
{{--    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>--}}
    <script type="text/javascript">
        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            // container.find('.select2-elem').each(function () {
            //     $(this).select2({
            //         placeholder: 'Виберіть елемент'
            //     });
            // });
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
