<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <div class="col-lg-11 input-group-sm">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][title]" placeholder="Заголовок" class="form-control mb-1" disabled>
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][subtitle]" placeholder="Подзаголовок" class="form-control mb-1" disabled>
                    <div class="row">
                        <div class="col-sm-4">
                            <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text1]" placeholder="Колонка 1" class="summernote form-control" disabled></textarea>
                        </div>
                        <div class="col-sm-4">
                            <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text2]" placeholder="Колонка 2" class="summernote form-control" disabled></textarea>
                        </div>
                        <div class="col-sm-4">
                            <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text3]" placeholder="Колонка 3" class="summernote form-control" disabled></textarea>
                        </div>
                    </div>


                </div>

                <div class="col-lg-1">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">

                    <div class="col-lg-11 input-group-sm">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][title]" placeholder="Заголовок" value="{{ old($field['name'] . '.' . $key . '.title', $value['title'] ?? '') }}" class="form-control mb-1">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][subtitle]" placeholder="Подзаголовок" value="{{ old($field['name'] . '.' . $key . '.subtitle', $value['subtitle'] ?? '') }}" class="form-control mb-1">
                        <div class="row">
                            <div class="col-sm-4">
                                <textarea name="{{ $field['name'] }}[{{ $key }}][text1]" placeholder="Колонка 1" class="summernote form-control">{{ old($field['name'] . '.' . $key . '.text1', $value['text1'] ?? '') }}</textarea>
                            </div>
                            <div class="col-sm-4">
                                <textarea name="{{ $field['name'] }}[{{ $key }}][text2]" placeholder="Колонка 2" class="summernote form-control">{{ old($field['name'] . '.' . $key . '.text2', $value['text2'] ?? '') }}</textarea>
                            </div>
                            <div class="col-sm-4">
                                <textarea name="{{ $field['name'] }}[{{ $key }}][text3]" placeholder="Колонка 3" class="summernote form-control">{{ old($field['name'] . '.' . $key . '.text3', $value['text3'] ?? '') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-1">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });


        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
