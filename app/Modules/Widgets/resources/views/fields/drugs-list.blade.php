<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">

                <div class="col-lg-11 input-group-sm">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][symbol]" placeholder="Символ" class="form-control mb-1" disabled>
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][title]" placeholder="Заголовок" class="form-control mb-1" disabled>
                    <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text]" placeholder="Текст" class="summernote form-control" disabled></textarea>
                    @include('admin.widget-fields.interlink.interlink',[
                        'name_type' => $field['name'] .'[#dynamicListPlaceholder][interlink_type]',
                        'name_val' => $field['name'] .'[#dynamicListPlaceholder][interlink_val]',
                    ])
                </div>

                <div class="col-lg-1">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">

                    <div class="col-lg-11 input-group-sm">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][symbol]" placeholder="Символ" value="{{ old($field['name'] . '.' . $key . '.symbol', $value['symbol'] ?? '') }}" class="form-control mb-1">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][title]" placeholder="Заголовок" value="{{ old($field['name'] . '.' . $key . '.title', $value['title'] ?? '') }}" class="form-control mb-1">
                        <textarea name="{{ $field['name'] }}[{{ $key }}][text]" placeholder="Текст" class="summernote form-control">{{ old($field['name'] . '.' . $key . '.text', $value['text'] ?? '') }}</textarea>
                        <?php
                            $interlinkSelType = old($field['name'] . '.' . $key . '.interlink_type', $value['interlink_type'] ?? '');
                            $interlinkSelVal = old($field['name'] . '.' . $key . '.interlink_val', $value['interlink_val'] ?? '');
                        ?>
                        @include('admin.widget-fields.interlink.interlink',[
                            'name_type' => $field['name'] .'['.$key.'][interlink_type]',
                            'name_val' => $field['name'] .'['.$key.'][interlink_val]',
                            'sel_type' => $interlinkSelType,
                            'sel_val' => $interlinkSelVal,
                        ])
                    </div>

                    <div class="col-lg-1">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field').each(function () {
                $(this).select2({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $('.items-container').find('.select2-field').each(function () {
            $(this).select2({});
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
