<?php

namespace App\Modules\Widgets\Models;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    /**
     * @var string
     */
    protected $table = 'widgets';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'main_id',
        'instance',
        'data',
        'lang',
        'static'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];

    public function isExistsLang($lang): bool
    {
        return self::query()
            ->where('main_id',$this->main_id)
            ->where('lang',$lang)
            ->exists();
    }

    public function getLangId($lang)
    {
        $m = self::query()
            ->where('main_id',$this->main_id)
            ->where('lang',$lang)
            ->first();

        return $m->id ?? null;
    }

    public function setMainId()
    {
        $maxMain = self::query()->orderBy('main_id','desc')->first();

        if($maxMain && isset($maxMain->main_id) && $maxMain->main_id){
            $maxMainId = $maxMain->main_id;
        } else {
            $maxMainId = 0;
        }

        $this->main_id = ($maxMainId + 1);
        return $this->save();
    }
}
