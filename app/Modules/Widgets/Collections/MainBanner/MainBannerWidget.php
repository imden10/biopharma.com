<?php

namespace App\Modules\Widgets\Collections\MainBanner;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class MainBannerWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Баннер для главной';

    public static string $preview = 'main-banner.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.main-banner.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image_mob',
                'label' => 'Изображение (для моб)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'quote',
                'label' => 'Цитата',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'fio',
                'label' => 'ФИО',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'position',
                'label' => 'Посада',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'photo',
                'label' => 'Фото',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'link',
                'label' => 'Ссылка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
