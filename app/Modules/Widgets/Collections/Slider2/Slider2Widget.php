<?php

namespace App\Modules\Widgets\Collections\Slider2;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class Slider2Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Слайдер (тип 2)';

    public static string $preview = 'slider-2.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.slider-2.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            [
                'type'  => 'title-image-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id   = $data['link']['interlink_val'][$type];

        $data = [
            'title'    => $data['title'],
            'subtitle' => $data['subtitle'],
            'theme'    => $data['theme'],
            'btn_name' => $data['btn_name'],
            'link'     => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang),
            'list'     => $data['list']
        ];

        $list = [];

        foreach ($data['list'] as $item) {
            $list[] = $item;
        }

        $data['list'] = $list;

        return $data;
    }
}
