<?php

namespace App\Modules\Widgets\Collections\News2;

use App\Models\BlogArticles;
use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class News2Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Новости (тип 2)';

    public static string $preview = 'news-2.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.news-2.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            [
                'type'  => 'news-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id   = $data['link']['interlink_val'][$type];

        $data = [
            'title'    => $data['title'],
            'theme'    => $data['theme'],
            'btn_name' => $data['btn_name'],
            'link'     => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang),
            'list'     => $data['list']
        ];

        $ids  = [];
        $list = [];

        if (!empty($data['list'])) {
            foreach ($data['list'] as $item) {
                $ids[] = (int)$item['article_id'];
            }
        }

        if (count($ids)) {
            $articles = BlogArticles::query()
                ->whereIn('id', $ids)
                ->active()
                ->get();

            $i = 0;

            if (count($articles)) {
                foreach ($data['list'] as $item1) {
                    foreach ($articles as $key => $item) {
                        if ($item1['article_id'] == $item->id) {
                            $cat = $item->tags[0]->slug;

                            $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_BLOG]['url_prefix'] . $cat . '/' .  $item->slug;

                            if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                $url = '/' . $lang . '/' . $url;
                            } else {
                                $url = '/' . $url;
                            }

                            $list[$i]['url'] = $url;

                            foreach ($item->translations as $trans) {
                                if ($trans->lang === $lang) {
                                    $list[$i]['name'] = $trans->name;
                                    $list[$i]['file'] = $trans->file;
                                    $list[$i]['text'] = $trans->text;
                                }
                            }

                            $list[$i]['image']       = $item->image;
                            $list[$i]['public_date'] = $item->public_date;

                            if (isset($item->tags[0])) {
                                $catUrl = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_BLOG]['url_prefix'] . $item->tags[0]->slug;

                                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                    $catUrl = '/' . $lang . '/' . $catUrl;
                                } else {
                                    $catUrl = '/' . $catUrl;
                                }

                                $catName = $item->tags[0]->name;

                                foreach ($item->tags[0]->translations as $trans) {
                                    if ($trans->lang === $lang) {
                                        $catName = $trans->name;
                                    }
                                }


                                $list[$i]['category'] = [
                                    'name' => $catName,
                                    'url'  => $catUrl
                                ];
                            }
                            $i++;
                        }
                    }
                }
            }
        }

        $data['list'] = $list;

        return $data;
    }
}
