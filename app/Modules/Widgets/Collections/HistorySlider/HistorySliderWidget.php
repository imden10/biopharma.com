<?php

namespace App\Modules\Widgets\Collections\HistorySlider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class HistorySliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'История слайдер';

    public static string $preview = 'history-slider.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.history-slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'history-slider-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item) {
            $type   = $item['interlink_type'];
            $id     = $item['interlink_val'][$type];
            $list[] = [
                'image' => $item['image'],
                'title' => $item['title'],
                'year'  => $item['year'],
                'text1' => $item['text1'],
                'text2' => $item['text2'],
                'text3' => $item['text3'],
                'link'  => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
