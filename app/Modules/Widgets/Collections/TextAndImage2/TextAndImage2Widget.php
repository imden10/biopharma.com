<?php

namespace App\Modules\Widgets\Collections\TextAndImage2;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class TextAndImage2Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Изображение и текст (тип 2)';

    public static string $preview = 'text-and-image-2.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.text-and-image-2.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'editor',
                'name'  => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:3000',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:3000',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'img_size',
                'label' => 'Размер изображения',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'small' => 'Маленькое',
                        'big'   => 'Большое'
                    ];
                }
            ],
            [
                'type'  => 'select',
                'name'  => 'img_position',
                'label' => 'Позиция изображения',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'left'  => 'Слева',
                        'right' => 'Справа'
                    ];
                }
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id   = $data['link']['interlink_val'][$type];

        $data = [
            'theme'        => $data['theme'],
            'title'        => $data['title'],
            'text'         => $data['text'],
            'btn_name'     => $data['btn_name'],
            'image'        => $data['image'],
            'img_size'     => $data['img_size'],
            'img_position' => $data['img_position'],
            'link'         => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
        ];

        return $data;
    }
}
