<?php

namespace App\Modules\Widgets\Collections\TitleTextLinkQuote;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class TitleTextLinkQuoteWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Заголовок с текстом и ссылкой и цитата';

    public static string $preview = 'title-text-link-quote.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.title-text-link-quote.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'title2',
                'label' => 'Заголовок 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'editor',
                'name'  => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:4000',
                'value' => '',
            ],
            [
                'type'  => 'editor',
                'name'  => 'quote',
                'label' => 'Цитата',
                'class' => '',
                'rules' => 'nullable|string|max:4000',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись на ссылке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка',
                'rules' => 'nullable|array',
                'value' => [],
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id   = $data['link']['interlink_val'][$type];

        $data = [
            'title'    => $data['title'],
            'title2'   => $data['title2'],
            'theme'    => $data['theme'],
            'btn_name' => $data['btn_name'],
            'text'     => $data['text'],
            'quote'    => $data['quote'],
            'link'     => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
        ];

        return $data;
    }
}
