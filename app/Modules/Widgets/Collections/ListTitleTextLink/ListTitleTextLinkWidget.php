<?php

namespace App\Modules\Widgets\Collections\ListTitleTextLink;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class ListTitleTextLinkWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Список (заголовок-текст-ссылка)';

    public static string $preview = 'list-title-text-link.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.list-title-text-link.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'title-text-btn_name-interlink-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item) {
            $type   = $item['interlink_type'];
            $id     = $item['interlink_val'][$type];
            $list[] = [
                'title'    => $item['title'],
                'text'     => $item['text'],
                'btn_name' => $item['btn_name'],
                'link'     => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
