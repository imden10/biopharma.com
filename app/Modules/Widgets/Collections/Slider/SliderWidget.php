<?php

namespace App\Modules\Widgets\Collections\Slider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class SliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Слайдер';

    public static string $preview = 'slider.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'slider-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

//    public function adapter($data, $lang)
//    {
//        $list = [];
//
//        foreach ($data['list'] as $item) {
//            $list[] = [
//                'image'    => $item['image'],
//            ];
//        }
//
//        $data['list'] = $list;
//
//        return $data;
//    }
}
