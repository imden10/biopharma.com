<?php

namespace App\Modules\Widgets\Collections\Accordion;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class AccordionWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Аккордеон';

    public static string $preview = 'accordion.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.accordion.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'title-text-interlink-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item){
            $type = $item['interlink_type'];
            $id = $item['interlink_val'][$type];
            $list[] = [
                'title' => $item['title'],
                'text' => $item['text'],
                'link' => app(Adapter::class)->getLinkByInterlinkData($type,$id,$lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
