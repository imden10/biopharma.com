<?php

namespace App\Modules\Widgets\Collections\DonorCenters;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class DonorCentersWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Донорські центри';

    public static string $preview = 'donor-centers.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.donor-centers.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'placeholder',
                'label' => 'Подсказка для поиска',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'donor-canters-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        if(isset($data['list']) && count($data['list'])){
            foreach ($data['list'] as $item){
                $list[] = [
                    'city' => $item['city'],
                    'text' => $item['text'],
                    'socials' => [
                        [
                            'name' => 'Facebook',
                            'link' => $item['facebook'],
                        ],
                        [
                            'name' => 'Youtube',
                            'link' => $item['youtube'],
                        ],
                        [
                            'name' => 'Instagram',
                            'link' => $item['instagram'],
                        ],
                    ],
                    'phones' => [
                        [
                            'name' => $item['phone1'],
                            'link' => 'tel:' . str_replace(['+',' ','(',')','-'],['','','','','',],$item['phone1']),
                        ],
                        [
                            'name' => $item['phone2'],
                            'link' => 'tel:' . str_replace(['+',' ','(',')','-'],['','','','','',],$item['phone2']),
                        ],
                    ]
                ];
            }
        }

        $data['list'] = $list;

        return $data;
    }
}
