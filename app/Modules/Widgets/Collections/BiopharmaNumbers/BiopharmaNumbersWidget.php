<?php

namespace App\Modules\Widgets\Collections\BiopharmaNumbers;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class BiopharmaNumbersWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Біофарма в цифрах';

    public static string $preview = 'biopharma-numbers.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.biopharma-numbers.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list' => function(){
                    return [
                        'light' => 'Светлая',
                        'gray' => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'title-btn_name-interlink-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item){
            $type = $item['interlink_type'];
            $id = $item['interlink_val'][$type];
            $list[] = [
                'btn_name' => $item['btn_name'],
                'text' => $item['text'],
                'link' => app(Adapter::class)->getLinkByInterlinkData($type,$id,$lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
