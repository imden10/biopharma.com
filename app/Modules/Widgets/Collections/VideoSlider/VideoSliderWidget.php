<?php

namespace App\Modules\Widgets\Collections\VideoSlider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class VideoSliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Видео слайдер';

    public static string $preview = 'video-slider.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.video-slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'video-slider-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item) {
            $type = $item['interlink_type'];
            $id   = $item['interlink_val'][$type];

            try {
                $query = '';
                $parts = null;
                $parts = parse_url($item['video']);
                parse_str($parts['query'], $query);
            } catch (\Exception $e) {

            }

            $list[] = [
                'image'        => $item['image'],
                'title'        => $item['title'],
                'video'        => $query['v'] ?? null,
                'video_length' => $item['video_length'] ?? null,
                'text'         => $item['text'],
                'link'         => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
