<?php

namespace App\Modules\Widgets\Collections\VideoSlider2;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class VideoSlider2Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Видео слайдер (тип 2)';

    public static string $preview = 'video-slider-2.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.video-slider-2.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'video-slider-list',
                'name'  => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item) {
            $type   = $item['interlink_type'];
            $id     = $item['interlink_val'][$type];
            $list[] = [
                'image' => $item['image'],
                'title' => $item['title'],
                'video' => $item['video'],
                'text'  => $item['text'],
                'link'  => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang)
            ];
        }

        $data['list'] = $list;

        return $data;
    }
}
