<?php

namespace App\Modules\Widgets\Collections\Accordion2;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class Accordion2Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Аккордеон (3 колонки)';

    public static string $preview = 'accordion-2.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.accordion-2.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list' => function(){
                    return [
                        'light' => 'Светлая',
                        'gray' => 'Темная'
                    ];
                }
            ],
            [
                'type' => 'accordion-2-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item){
            $list[] = $item;
        }

        $data['list'] = $list;

        return $data;
    }
}
