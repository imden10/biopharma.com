<?php

namespace App\Modules\Widgets\Collections\FormLinks;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class FormLinksWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Форми звернення онлайн';

    public static string $preview = 'form-links.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.form-links.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'theme',
                'label' => 'Тема',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'light' => 'Светлая',
                        'gray'  => 'Темная'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись на кнопке 1',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка 1',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name2',
                'label' => 'Надпись на кнопке 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link2',
                'label' => 'Ссылка 2',
                'rules' => 'nullable|array',
                'value' => [],
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id   = $data['link']['interlink_val'][$type];

        $type2 = $data['link2']['interlink_type'];
        $id2   = $data['link2']['interlink_val'][$type];

        $data = [
            'title'     => $data['title'],
            'theme'     => $data['theme'],
            'btn_name'  => $data['btn_name'],
            'link'      => app(Adapter::class)->getLinkByInterlinkData($type, $id, $lang),
            'btn_name2' => $data['btn_name2'],
            'link2'     => app(Adapter::class)->getLinkByInterlinkData($type2, $id2, $lang),
        ];

        return $data;
    }
}
