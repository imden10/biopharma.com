<?php

namespace App\Modules\Widgets\Collections\ImageAndLink;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class ImageAndLinkWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Картинка и ссылка';

    public static string $preview = 'image-and-link.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.image-and-link.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'interlink',
                'name'  => 'link',
                'label' => 'Ссылка',
                'rules' => 'nullable|array',
                'value' => [],
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $type = $data['link']['interlink_type'];
        $id = $data['link']['interlink_val'][$type];

        $data = [
            'title' => $data['title'],
            'image' => $data['image'],
            'link' => app(Adapter::class)->getLinkByInterlinkData($type,$id,$lang)
        ];

        return $data;
    }
}
