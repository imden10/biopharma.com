<?php

namespace App\Modules\Widgets\Collections\ContactForm;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;

class ContactFormWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Форма обратной связи';

    public static string $preview = 'contact-form.jpg';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.contact-form.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле имя'],
            [
                'type'  => 'text',
                'name'  => 'field_name_name',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'field_name_placeholder',
                'label' => 'Подсказка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле компания'],
            [
                'type'  => 'text',
                'name'  => 'field_company_placeholder',
                'label' => 'Подсказка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле телефон'],
            [
                'type'  => 'text',
                'name'  => 'field_phone_name',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'field_phone_placeholder',
                'label' => 'Подсказка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле E-mail'],
            [
                'type'  => 'text',
                'name'  => 'field_email_placeholder',
                'label' => 'Подсказка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле описание'],
            [
                'type'  => 'select',
                'name'  => 'field_description_visible',
                'label' => 'Статус',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        '0'  => 'Не показывать',
                        '1'  => 'Показывать'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'field_description_placeholder',
                'label' => 'Подсказка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле дата'],
            [
                'type'  => 'select',
                'name'  => 'field_date_visible',
                'label' => 'Статус',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        '0'  => 'Не показывать',
                        '1'  => 'Показывать'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'field_date_name',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поле список'],
            [
                'type'  => 'select',
                'name'  => 'field_list_visible',
                'label' => 'Статус',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        '0'  => 'Не показывать',
                        '1'  => 'Показывать'
                    ];
                }
            ],
            [
                'type'  => 'text',
                'name'  => 'field_list_name',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Кнопка'],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Надпись',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
