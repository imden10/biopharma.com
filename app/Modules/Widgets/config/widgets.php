<?php

return [

    /*
    |------------------------------------------------------------------
    | View layout component
    |------------------------------------------------------------------
    */
    //    'view-layout' => 'admin::layout',
    'view-layout'  => 'admin::layout',

    /*
    |------------------------------------------------------------------
    | View layout title
    |------------------------------------------------------------------
    */
    'layout-title' => 'widgets::strings.layout.title',


    'widgets'           => [
        'main-banner'             => \App\Modules\Widgets\Collections\MainBanner\MainBannerWidget::class,
        'sections-often-searched' => \App\Modules\Widgets\Collections\SectionsOftenSearched\SectionsOftenSearchedWidget::class,
        'what-is-biopharma'       => \App\Modules\Widgets\Collections\WhatIsBiopharma\WhatIsBiopharmaWidget::class,
        'video-slider'            => \App\Modules\Widgets\Collections\VideoSlider\VideoSliderWidget::class,
        'video-slider-2'          => \App\Modules\Widgets\Collections\VideoSlider2\VideoSlider2Widget::class,
        'accordion'               => \App\Modules\Widgets\Collections\Accordion\AccordionWidget::class,
        'accordion-2'             => \App\Modules\Widgets\Collections\Accordion2\Accordion2Widget::class,
        'image-and-link'          => \App\Modules\Widgets\Collections\ImageAndLink\ImageAndLinkWidget::class,
        'image-and-link-2'        => \App\Modules\Widgets\Collections\ImageAndLink2\ImageAndLink2Widget::class,
        'history-slider'          => \App\Modules\Widgets\Collections\HistorySlider\HistorySliderWidget::class,
        'see-also-main'           => \App\Modules\Widgets\Collections\SeeAlsoMain\SeeAlsoMainWidget::class,
        'video-banner'            => \App\Modules\Widgets\Collections\VideoBanner\VideoBannerWidget::class,
        'drugs'                   => \App\Modules\Widgets\Collections\Drugs\DrugsWidget::class,
        'slider-tabs'             => \App\Modules\Widgets\Collections\SliderTabs\SliderTabsWidget::class,
        'see-also-type-1'         => \App\Modules\Widgets\Collections\SeeAlsoType1\SeeAlsoType1Widget::class,
        'list-title-text-link'    => \App\Modules\Widgets\Collections\ListTitleTextLink\ListTitleTextLinkWidget::class,
        'video-banner-2'          => \App\Modules\Widgets\Collections\VideoBanner2\VideoBanner2Widget::class,
        'image-title-text-link'   => \App\Modules\Widgets\Collections\ImageTitleTextLink\ImageTitleTextLinkWidget::class,
        'see-also-type-2'         => \App\Modules\Widgets\Collections\SeeAlsoType2\SeeAlsoType2Widget::class,
        'biopharma-socials'       => \App\Modules\Widgets\Collections\BiopharmaSocials\BiopharmaSocialsWidget::class,
        'text-element'                    => \App\Modules\Widgets\Collections\TextElement\TextElementWidget::class,
        'text-2'                  => \App\Modules\Widgets\Collections\Text2\Text2Widget::class,
        'biopharma-numbers'       => \App\Modules\Widgets\Collections\BiopharmaNumbers\BiopharmaNumbersWidget::class,
        'text-and-image'          => \App\Modules\Widgets\Collections\TextAndImage\TextAndImageWidget::class,
        'text-and-image-2'        => \App\Modules\Widgets\Collections\TextAndImage2\TextAndImage2Widget::class,
        'sections'                => \App\Modules\Widgets\Collections\Sections\SectionsWidget::class,
        'news'                    => \App\Modules\Widgets\Collections\News\NewsWidget::class,
        'news-2'                  => \App\Modules\Widgets\Collections\News2\News2Widget::class,
        'title-text-link'         => \App\Modules\Widgets\Collections\TitleTextLink\TitleTextLinkWidget::class,
        'title-text-link-quote'   => \App\Modules\Widgets\Collections\TitleTextLinkQuote\TitleTextLinkQuoteWidget::class,
        'full-image'              => \App\Modules\Widgets\Collections\FullImage\FullImageWidget::class,
        'text-3-column'           => \App\Modules\Widgets\Collections\Text3Column\Text3ColumnWidget::class,
        'slider'                  => \App\Modules\Widgets\Collections\Slider\SliderWidget::class,
        'slider-2'                => \App\Modules\Widgets\Collections\Slider2\Slider2Widget::class,
        'form-links'              => \App\Modules\Widgets\Collections\FormLinks\FormLinksWidget::class,
        'contacts-menu'           => \App\Modules\Widgets\Collections\ContactsMenu\ContactsMenuWidget::class,
        'contacts-list'           => \App\Modules\Widgets\Collections\ContactsList\ContactsListWidget::class,
        'contact-form'            => \App\Modules\Widgets\Collections\ContactForm\ContactFormWidget::class,
        'donor-centers'           => \App\Modules\Widgets\Collections\DonorCenters\DonorCentersWidget::class,
    ],

    /*
    |------------------------------------------------------------------
    | Permissions for manipulation widgets
    |------------------------------------------------------------------
    */
    'permissions'       => [
        'widgets.view'   => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.create' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.update' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.delete' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
    ],

    /*
    |------------------------------------------------------------------
    | Middleware for settings
    |------------------------------------------------------------------
    */
    'middleware'        => ['web', 'auth', 'verified'],

    /*
    |------------------------------------------------------------------
    | Uri Route prefix
    |------------------------------------------------------------------
    */
    'uri_prefix'        => 'admin',

    /*
    |------------------------------------------------------------------
    | Route name prefix
    |------------------------------------------------------------------
    */
    'route_name_prefix' => 'admin.',

    /*
    |------------------------------------------------------------------
    | Request lang key
    |------------------------------------------------------------------
    */
    'request_lang_key'  => 'lang',

];
