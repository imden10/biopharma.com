<?php

use App\Modules\Widgets\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => config('widgets.uri_prefix', 'admin'),
    'as' => config('widgets.route_name_prefix', 'admin.'),
    'middleware' => config('widgets.middleware', []),
], function () {
    Route::get('widgets/copy/{from_id}/{to_lang}', [CRUDController::class,'copy']);
    Route::resource('widgets', CRUDController::class)->except('show');
});
