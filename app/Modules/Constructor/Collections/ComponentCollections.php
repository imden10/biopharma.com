<?php

namespace App\Modules\Constructor\Collections;

class ComponentCollections
{
    /**
     * @return array
     */
    public static function widget()
    {
        return [
            'label'  => 'Виджет',
            'params' => [
                'labels'  => [

                ],
                'widgets' => function ($lang) {
                    $widgets = \App\Modules\Widgets\Models\Widget::where('lang', $lang)->get();

                    return ['' => '---'] + collect($widgets)
                            ->mapWithKeys(function ($widget) {
                                return [$widget->id => $widget->name];
                            })
                            ->toArray();
                },
                'shown_name' => 'widget'
            ],
            'rules'  => [
                'content.widget' => 'nullable|integer',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageAndText()
    {
        return [
            'label'  => 'Изображение и текст',
            'params' => [
                'image_positions' => [
                    'right' => 'Праворуч',
                    'left'  => 'Ліворуч',
                ],
                'labels'          => [
                    'title'          => 'Заголовок',
                    'image_position' => 'Позиція зображення',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'          => 'nullable|string|max:255',
                'content.image'          => 'nullable|string|max:255',
                'content.image_position' => 'required|string|max:255',
                'content.description'    => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function simpleText()
    {
        return [
            'label'  => 'Текст',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.description' => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function list()
    {
        return [
            'label'  => 'Список',
            'params' => [
                'type'   => [
                    'ul' => 'Обычный',
                    'ol' => 'Нумерованый'
                ],
                'labels' => [
                    'type'  => 'Тип',
                    'item'  => 'Текст',
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.type'        => 'required|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageElement()
    {
        return [
            'label'  => 'Изображение',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.image' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function quotes()
    {
        return [
            'label'  => 'Цитата',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'text'  => 'Текст',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.text'  => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function gallery()
    {
        return [
            'label'  => 'Галерея',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'image' => 'Изображение',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function questionAnswer()
    {
        return [
            'label'  => 'Вопрос-ответ',
            'params' => [
                'labels' => [
                    'title' => 'Вопрос',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function anchorMenu()
    {
        return [
            'label'  => 'Анкер-Меню',
            'params' => [
                'labels' => [
                    'title' => 'Заголовок',
                    'link' => 'ID виджета',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function seeAlso()
    {
        return [
            'label'  => 'Смотрите также',
            'params' => [
                'labels' => [
                    'name' => 'Заголовок',
                    'title' => 'Выберите страницу',
                ],
                'shown_name' => 'name'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

}

