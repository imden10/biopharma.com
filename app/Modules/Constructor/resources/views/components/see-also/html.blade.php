@include('constructor::layouts.header',['lang' => $lang])

<?php
$pages = \App\Models\Pages::query()->active()->get();
?>

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="see-also-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-5">
                        <input type="text" placeholder="{{ $params['labels']['name'] }}" class="form-control" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][name]" disabled>
                    </div>

                    <div class="col-5">
                        <select name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][page_id]" class="select2-field2" style="width: 100%">
                            <option value="">{{ $params['labels']['title'] }}</option>
                            @foreach($pages as $page)
                                <option value="{{$page->id}}">{{$page->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="see-also-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-5">
                            <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][name]" placeholder="{{ $params['labels']['name'] }}" class="form-control mt-3 mb-1" value="{{ $value['name'] ?? '' }}">
                        </div>
                        <div class="col-5">
                            <select name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][page_id]" class="select2-field2-shown">
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}" @if(($value['page_id'] ?? '') == $page->id) selected @endif>{{$page->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-see-also-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
