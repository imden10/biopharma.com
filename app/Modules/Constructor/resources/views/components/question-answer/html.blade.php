@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="question-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-10">
                        <select name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][faq_id]" class="select2-field2">
                            @foreach(\App\Models\Faq::query()->get() as $item)
                                <option value="{{$item->id}}">{{$item->question}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="question-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-10">
                            <select name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][faq_id]" class="select2-field2-shown">
                                @foreach(\App\Models\Faq::query()->get() as $item)
                                    <option value="{{$item->id}}" @if(($value['faq_id'] ?? '') == $item->id) selected @endif>{{$item->question}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-question-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
