<?php


namespace App\Modules\Menu;

use Illuminate\Support\Facades\DB;

class Menu
{
    public static function get($name)
    {
//        DB::enableQueryLog();

        $model = \App\Models\Menu::query()
            ->where('tag', $name)
            ->where('const', '<>', 1)
            ->with(['page','blog','landing','donor','plasmacentr'])
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

//        dd(DB::getQueryLog());

        return count($model) ? $model : [];
    }
}
