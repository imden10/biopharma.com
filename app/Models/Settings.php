<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Settings
 * @package App\Models
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $value
 * @property boolean $const
 * @property string $lang
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Settings extends Model
{
    use HasFactory;

    protected $table = 'settings';

    protected $guarded = [];

    protected $casts = [
        'const' => 'boolean'
    ];

    const TAB_MAIN     = 'main';
    const TAB_BLOG     = 'blog';
    const TAB_PRODUCTS = 'products';
    const TAB_FOOTER   = 'footer';
    const TAB_FORMS    = 'forms';

    public static function getTabNames(): array
    {
        return [
            self::TAB_MAIN     => 'Главные',
            self::TAB_BLOG     => 'Блог',
            self::TAB_PRODUCTS => 'Продукты',
            self::TAB_FOOTER   => 'Футер',
            self::TAB_FORMS    => 'Формы',
        ];
    }
}
