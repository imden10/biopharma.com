<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Roles
 * @package App
 *
 * @property string $name
 * @property string $code
 */

class Roles extends Model
{
    protected $table = 'roles';

    protected $guarded = [];
}
