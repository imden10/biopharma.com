<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormConsumer extends Model
{
    use HasFactory;

    protected $table = 'form_consumers';

    protected $guarded = [];
}
