<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogArticleTranslation extends Model implements HasConstructor
{
    use HasFactory;
    use Constructorable;

    protected $table = 'blog_article_translations';

    private string $entityAttribute = 'blog_articles_id';


    protected $fillable = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'file',
    ];

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    public $timestamps = false;

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
            'simple-text'   => ComponentCollections::simpleText(),
            'quotes'        => ComponentCollections::quotes(),
            'list'          => ComponentCollections::list(),
            'image-element' => ComponentCollections::imageElement(),
            'gallery'       => ComponentCollections::gallery(),
            'widget'        => ComponentCollections::widget(),
        ];
    }
}
