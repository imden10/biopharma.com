<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogTagTranslation extends Model
{
    use HasFactory;

    protected $table = 'blog_tag_translations';

    protected $fillable = [
        'name',
        'meta_title',
        'meta_description',
    ];

    public $timestamps = false;

    private string $entityAttribute = 'blog_tags_id';
}
