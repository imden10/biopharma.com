<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductTranslation
 * @package App\Models\Translations
 *
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $instruction
 * @property string $release_form
 * @property string $package
 * @property string $composition
 * @property string $age
 * @property string $indication
 * @property string $catalogIndication
 * @property string $contraindications
 * @property string $publication
 * @property string $storage_conditions
 */
class ProductTranslation extends Model
{
    use HasFactory;

    protected $table = 'product_translations';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'meta_title',
        'meta_description',
        'instruction',
        'release_form',
        'package',
        'composition',
        'age',
        'indication',
        'publication',
        'catalogIndication',
        'contraindications',
        'storage_conditions'
    ];

}
