<?php

namespace App\Models;

use App\Models\Translations\BlogTagTranslation;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class BlogTags
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 *
 * @property BlogArticles[] $articles
 */
class BlogTags extends Model
{
    use HasFactory;
    use Sluggable;
    use Translatable;

    protected $table = 'blog_tags';

    public $translationModel = 'App\Models\Translations\BlogTagTranslation';

    public $translatedAttributes = [
        'name',
        'meta_title',
        'meta_description',
    ];

    protected $fillable = [
        'slug'
    ];

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return BlogTagTranslation::query()
            ->where('blog_tags_id',$this->id)
            ->where('lang',$lang)
            ->where('name','<>','')
            ->exists();
    }

    /**
     * @return BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(BlogArticles::class, 'blog_article_tag', 'blog_tag_id', 'blog_article_id')
            ->where('status',1);
    }

}
