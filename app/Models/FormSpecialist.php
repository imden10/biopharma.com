<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormSpecialist extends Model
{
    use HasFactory;

    protected $table = 'form_specialists';

    protected $fillable = [
        'name',
        'date',
        'illNo',
        'gender',
        'weight',
        'description',
        'startDate',
        'startTime',
        'endDate',
        'endTime',
        'naslidki',
        'naslidkiExtra',
        'categoriesPr',
        'categoriesPrExtra',
        'prodName',
        'manufacturer',
        'prodNo',
        'testimony',
        'dose',
        'multiplicity',
        'drugAdministration',
        'terapiaStart',
        'terapiaStartUndefined',
        'terapiaEnd',
        'terapiaEndUndefined',
        'prodItems',
        'PLZCancel',
        'PLZCancelingPR',
        'reassignmentPLZ',
        'PRRecoverAfterPLZ',
        'changingTheDoseRegimenOfPLZ',
        'changingTheDoseRegimenOfPLZtext',
        'PR_VE',
        'additionInformation',
        'informerName',
        'informerPhoneFax',
        'informerEmail',
        'informerClinicName',
        'informerReporter'
    ];
}
