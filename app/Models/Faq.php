<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Faq
 * @package App\Models
 *
 * @property integer $id
 * @property integer $audio_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Faq extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'faqs';

    public $translationModel = 'App\Models\Translations\FaqTranslation';

    public $translatedAttributes = [
        'question',
        'answer'
    ];

    protected $fillable = [
        'audio_id'
    ];

    public function audio()
    {
        return $this->hasOne(Audio::class,'id','audio_id');
    }
}
