<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Menu extends Model
{
    use NodeTrait;
    use Translatable;

    protected $table = "menu";

    public $translationModel = 'App\Models\Translations\MenuTranslation';

    public $translatedAttributes = [
        'name',
        'url'
    ];

    protected $fillable = [
        'visibility',
        'parent_id ',
        'tag',
        'const',
        'type',
        'model_id',
        'icon'
    ];

    public $timestamps = false;

    const VISIBILITY_OFF = 0;
    const VISIBILITY_ON  = 1;

    const TYPE_ARBITRARY     = 0;
    const TYPE_PAGE          = 1;
    const TYPE_BLOG          = 2;

    const INTER_TYPE_ARBITRARY     = 'arbitrary';
    const INTER_TYPE_PAGE          = 'page';
    const INTER_TYPE_BLOG          = 'article';

    public static function getVisibility(): array
    {
        return [
            self::VISIBILITY_OFF => 'Не показувати',
            self::VISIBILITY_ON  => 'Показувати'
        ];
    }

    /**
     * @return array
     */
    public static function getTags(): array
    {
        return self::query()
                ->where('const', 1)
                ->pluck('tag', 'tag')
                ->toArray() ?? [];
    }

    /**
     * @return array
     */
    public static function getTagsWithId(): array
    {
        return self::query()
                ->where('const', 1)
                ->pluck('tag', 'id')
                ->toArray() ?? [];
    }

    public static function getTypes(): array
    {
        return [
            self::TYPE_PAGE          => 'Страницы',
            self::TYPE_BLOG          => 'Новости',
            self::TYPE_ARBITRARY     => 'Произвольные ссылки'
        ];
    }

    public static function getTypesOne(): array
    {
        return [
            self::TYPE_PAGE          => 'Страница',
            self::TYPE_BLOG          => 'Новость',
            self::TYPE_ARBITRARY     => 'Произвольная ссылка'
        ];
    }

    public static function getTypesModel(): array
    {
        return [
            self::TYPE_PAGE    => [
                'rel'        => 'page',
                'name'       => 'title',
                'url_prefix' => ''
            ],
            self::TYPE_BLOG    => [
                'rel'        => 'blog',
                'name'       => 'name',
                'url_prefix' => 'news/'
            ]
        ];
    }

    public static function getInterTypesModel(): array
    {
        return [
            self::INTER_TYPE_PAGE    => [
                'rel'        => 'page',
                'name'       => 'title',
                'url_prefix' => ''
            ],
            self::INTER_TYPE_BLOG    => [
                'rel'        => 'blog',
                'name'       => 'name',
                'url_prefix' => 'news/'
            ]
        ];
    }

    public function page()
    {
        return $this->hasOne(Pages::class, 'id', 'model_id');
    }

    public function blog()
    {
        return $this->hasOne(BlogArticles::class, 'id', 'model_id');
    }
}
