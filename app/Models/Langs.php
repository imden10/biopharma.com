<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Langs
 * @package App\Models
 *
 * @property string $code
 * @property string $short_name
 * @property string $name
 * @property string $icon
 * @property boolean $default
 *
 * @property array $getLangCodes
 * @property string $getDefaultLangCode
 * @property array $getLangsWithTitle
 */
class Langs extends Model
{
    protected $table = 'langs';

    protected $guarded = [];

    protected $casts = [
        'default' => 'boolean'
    ];

    public $timestamps = false;

    /**
     * @return array
     */
    public static function getLangCodes(): array
    {
        return self::query()->pluck('code')->toArray();
    }

    /**
     * @return string
     */
    public static function getDefaultLangCode()
    {
        $model = self::query()->where('default',true)->first();
        return $model ? $model->code : null;
    }

    /**
     * @return array
     */
    public static function getLangsWithTitle(): array
    {
        return self::query()->orderBy('default','desc')->pluck('name','code')->toArray();
    }

    /**
     * @return array
     */
    public static function getLangsWithTitleShort(): array
    {
        return self::query()->orderBy('default','desc')->pluck('short_name','code')->toArray();
    }
}
