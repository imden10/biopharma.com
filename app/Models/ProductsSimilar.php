<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductsSimilar extends Pivot
{
    use HasFactory;

    protected $table = 'products_similar';

    public $timestamps = false;

    protected $guarded = [];
}
