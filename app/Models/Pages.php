<?php

namespace App\Models;

use App\Models\Translations\PagesTranslation;
use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Traits\Constructorable;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Modules\Constructor\Contracts\HasConstructor;

/**
 * Class Pages
 * @package App
 *
 * @property string $slug
 * @property integer $status
 * @property integer $parent_id
 * @property string $path
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property array $getStatuses
 */
class Pages extends Model
{
    use Sluggable;
    use Translatable;

    protected $table = 'pages';

    public $translationModel = 'App\Models\Translations\PagesTranslation';

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'image',
        'path',
        'parent_id',
        'type'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    const TYPE_PAGE = 'page';
    const TYPE_LANDING = 'landing';

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getTypes(): array
    {
        return [
            self::TYPE_PAGE => 'Страница',
            self::TYPE_LANDING => 'Лендинг',
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активная',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активная',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: '.self::getStatuses()[$this->status]["color"].'; background-color: '.self::getStatuses()[$this->status]["bg_color"].'">'.self::getStatuses()[$this->status]["title"].'</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('pages.status', self::STATUS_ACTIVE);
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return PagesTranslation::query()
            ->where('pages_id',$this->id)
            ->where('lang',$lang)
            ->where('title','<>','')
            ->exists();
    }

    public function parent() {
        return $this->belongsTo(self::class, 'parent_id');
    }

    private function getCategories($cat, $res = [])
    {
        if(isset($cat->parent->title)){
            $res[] = $cat->parent->title;
            return $this->getCategories($cat->parent,$res);
        }

        return $res;
    }

    public function getNameWithPath()
    {
        $arr = $this->getCategories($this);

        $arr = array_reverse($arr);

        $res = '';

        foreach ($arr as $item){
            $res .= $item . ' > ';
        }

        $res .= $this->title;

        return $res;
    }
}
