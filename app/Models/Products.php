<?php

namespace App\Models;

use App\Models\Translations\ProductTranslation;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Products
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 * @property string $image
 * @property string $order
 * @property string $status
 * @property integer $category_id
 *
 * @property Category $category
 *
 * @property Products[] $similar
 */
class Products extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'products';

    public $translationModel = 'App\Models\Translations\ProductTranslation';

    public $translatedAttributes = [
        'name',
        'description',
        'meta_title',
        'meta_description',
        'instruction',
        'release_form',
        'package',
        'composition',
        'age',
        'indication',
        'publication',
        'catalogIndication',
        'contraindications',
        'storage_conditions'
    ];

    protected $fillable = [
        'slug',
        'image',
        'order',
        'status',
        'category_id'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активный',
            self::STATUS_ACTIVE     => 'Активный'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     */
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function scopeActive($query)
    {
        return $query->where('products.status', self::STATUS_ACTIVE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function similar()
    {
        return $this->belongsToMany(Products::class,'products_similar','self_id','similar_id');
    }

    public function getSimilarModels($lang){
        $similar = $this->similar();
        $similarModels = $similar->leftJoin('category','category.id','=','products.category_id')
            ->leftJoin('category_translations','category_translations.category_id','=','products.category_id')
            ->where('category_translations.lang', $lang)
            ->select([
                'products.*',
                'category.slug AS categorySlug',
                'category_translations.title AS categoryName'
            ])
            ->active()
            ->get();

        return $similarModels;
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return ProductTranslation::query()
            ->where('products_id',$this->id)
            ->where('lang',$lang)
            ->where('name','<>','')
            ->exists();
    }

}
