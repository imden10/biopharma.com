<?php
$segment  = 1;
$allLangs = ['ru', 'uk', 'en'];
$route    = request()->segment($segment);

if (in_array($route, $allLangs)) {
    $segment = 2;
}

$route = request()->segment($segment);
$slug  = request()->segment($segment + 1);

$data = [];
$parts = [];

$url = $_SERVER['REQUEST_URI'];
$pathInfo = pathinfo($url);
if(isset($pathInfo['extension'])){
    return;
}

switch ($route) {
    case null:
        /* главная страница */
        $model = \App\Models\Pages::query()->where('id', 44)->first();

        if (!$model)
            break;

        $mr = '<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Фармацевтична компанія «Біофарма»",
  "url": "https://biopharma.ua/",
  "logo": "https://biopharma.ua/assets/app/img/logo.79ff9fda.svg",
  "telephone": "+380 (44) 390 08 10",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+380 (44) 390 08 10",
    "contactType": "customer service",
    "areaServed": "UA",
    "availableLanguage": ["Ukrainian","Russian","en"]
  },
  "address": {
	"@type": "PostalAddress",
	"streetAddress": "вул. Київська, 37",
	"addressLocality": "Біла Церква",
	"addressCountry": "Украина",
	"postalCode": "09100"
	},
      "sameAs": [
    "https://www.facebook.com/biopharma.ua",
    "https://www.youtube.com/channel/UCU2tFOZjF9dR5NRNzQSg8_Q"
  ]
}
</script>
';

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image),
            'mr'          => $mr
        ];
        break;
    case 'preparati':
        $url = url()->current();
        $parts = explode('preparati/',$url);

        /* все продукты */
        if(! $slug){
            $urlCurrent = url()->current();

            $lng = request()->segment(1);

            if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
                $ruUrl = $urlCurrent;
                $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
            } else {
                $defaultUrl = $urlCurrent;
                $urlAfterDomain = request()->path();

                $r = '/ru/' . $urlAfterDomain;
                $r = str_replace('//','',$r);
                $ruUrl = env('APP_URL') . $r;
            }

            $page = request()->input('page',"1");

            $afterTitle = '';

            switch ($page){
                case "1":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=2'>";
                    break;
                case "2":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=3'>";
                    break;
                default:
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."?page=".($page - 1)."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=".($page + 1)."'>";
                    break;
            }

            $data = [
                'title'       => app(Setting::class)->get('products_title'),
                'description' => app(Setting::class)->get('products_description'),
                'image'       => null
            ];

            if($data['title'] !== '' && $page > 1){
                $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
                $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
            }

        } elseif(isset($parts[1])) {
            $parts = explode('/',$parts[1]);

            // страница категории продуктов
            if(count($parts) == 1){
                $model = \App\Models\Category::query()->where('slug', $slug)->first();

                $urlCurrent = url()->current();

                $lng = request()->segment(1);

                if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
                    $ruUrl = $urlCurrent;
                    $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
                } else {
                    $defaultUrl = $urlCurrent;
                    $urlAfterDomain = request()->path();

                    $r = '/ru/' . $urlAfterDomain;
                    $r = str_replace('//','',$r);
                    $ruUrl = env('APP_URL') . $r;
                }

                $page = request()->input('page',"1");

                $afterTitle = '';

                switch ($page){
                    case "1":
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=2'>";
                        break;
                    case "2":
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=2'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=2'>" . PHP_EOL;
                        $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=3'>";
                        break;
                    default:
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=".$page."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=".$page."'>" . PHP_EOL;
                        $canonical .= "<link rel='prev' href='".$urlCurrent."?page=".($page - 1)."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=".($page + 1)."'>";
                        break;
                }

                $data = [
                    'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
                    'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
                    'image'       => get_image_uri($model->image)
                ];

                if($data['title'] !== '' && $page > 1){
                    $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
                    $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
                }
            }
            // страница продукта одного
            elseif(count($parts) == 2){
                $model = \App\Models\Products::query()->where('slug', $parts[1])->first();

                $data = [
                    'title'       => $model->getTranslation(app()->getLocale())->meta_title,
                    'description' => $model->getTranslation(app()->getLocale())->meta_description,
                    'image'       => get_image_uri($model->image)
                ];
            }

            if (! $model)
                break;
        }
        break;
    case 'farmakonaglyad':
        if($slug === 'consumers'){
            $data = [
                'title'       => app(Setting::class)->get('form_pharmakanyaglyad_default_title'),
                'description' => app(Setting::class)->get('form_pharmakanyaglyad_default_description'),
                'image'       => null
            ];
        } elseif($slug === 'specialists'){
            $data = [
                'title'       => app(Setting::class)->get('form_pharmakanyaglyad_title'),
                'description' => app(Setting::class)->get('form_pharmakanyaglyad_description'),
                'image'       => null
            ];
        }

        break;
    case 'news':
        $url = url()->current();
        $parts = explode('news/',$url);

        /* все новости */
        if(! $slug){
            $urlCurrent = url()->current();

            $lng = request()->segment(1);

            if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
                $ruUrl = $urlCurrent;
                $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
            } else {
                $defaultUrl = $urlCurrent;
                $urlAfterDomain = request()->path();

                $r = '/ru/' . $urlAfterDomain;
                $r = str_replace('//','',$r);
                $ruUrl = env('APP_URL') . $r;
            }

            $page = request()->input('page',"1");

            $afterTitle = '';

            switch ($page){
                case "1":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=2'>";
                    break;
                case "2":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=3'>";
                    break;
                default:
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."?page=".($page - 1)."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=".($page + 1)."'>";
                    break;
            }

            $data = [
                'title'       => app(Setting::class)->get('news_title'),
                'description' => app(Setting::class)->get('news_description'),
                'image'       => null
            ];

            if($data['title'] !== '' && $page > 1){
                $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
                $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
            }
        } elseif(isset($parts[1])) {
            $parts = explode('/',$parts[1]);

            // страница категории новостей
            if(count($parts) == 1){
                $model = \App\Models\BlogTags::query()->where('slug', $slug)->first();

                $urlCurrent = url()->current();

                $lng = request()->segment(1);

                if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
                    $ruUrl = $urlCurrent;
                    $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
                } else {
                    $defaultUrl = $urlCurrent;
                    $urlAfterDomain = request()->path();

                    $r = '/ru/' . $urlAfterDomain;
                    $r = str_replace('//','',$r);
                    $ruUrl = env('APP_URL') . $r;
                }

                $page = request()->input('page',"1");

                $afterTitle = '';

                switch ($page){
                    case "1":
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=2'>";
                        break;
                    case "2":
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=2'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=2'>" . PHP_EOL;
                        $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=3'>";
                        break;
                    default:
                        $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=".$page."'>" . PHP_EOL;
                        $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=".$page."'>" . PHP_EOL;
                        $canonical .= "<link rel='prev' href='".$urlCurrent."?page=".($page - 1)."'>" . PHP_EOL;
                        $canonical .= "<link rel='next' href='".$urlCurrent."?page=".($page + 1)."'>";
                        break;
                }

                $data = [
                    'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
                    'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
                    'image'       => get_image_uri($model->image)
                ];

                if($data['title'] !== '' && $page > 1){
                    $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
                    $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
                }
            }
            // страница одной новости
            elseif(count($parts) == 2){
                $model = \App\Models\BlogArticles::query()->where('slug', $parts[1])->first();
            }

            if (! $model)
                break;

            $data = [
                'title'       => $model->getTranslation(app()->getLocale())->meta_title,
                'description' => $model->getTranslation(app()->getLocale())->meta_description,
                'image'       => get_image_uri($model->image)
            ];
        }
        break;
    default:
        $model = \App\Models\Pages::query()->where('slug', $route)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
}
?>

@isset($data['title'])
    <title>{{$data['title']}}</title>
@else
    <title>{{env('APP_NAME')}}</title>
@endisset

<meta name="test" content="{{env('APP_URL') . '/media/images/original/images/no-image.png'}}">

@if(count($data))
    @if((! isset($data['image'])) || (isset($data['image']) && $data['image'] === env('APP_URL') . '/media/images/original/images/no-image.png'))
        <?php $data['image'] = get_image_uri(app(Setting::class)->get('default_og_image',\App\Models\Langs::getDefaultLangCode())); ?>
    @endif

    @isset($data['description'])
        <meta name="description" content="{{$data['description']}}">
    @endisset

    @isset($data['title'])
        <meta property="og:title" content="{{$data['title']}}"/>
    @else
        <meta property="og:title" content="{{env('APP_NAME')}}"/>
    @endisset

    @isset($data['description'])
        <meta property="og:description" content="{{$data['description']}}"/>
    @endisset

    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:site_name" content="{{env('APP_NAME')}}"/>
    @isset($data['image'])
        <meta property="og:image" content="{{$data['image']}}"/>
    @endisset
    @if(isset($data['mr']) && $data['mr'] != '')
        {!! $data['mr'] !!}
    @endif
@endif

{!!  app(Setting::class)->get('head_code',\App\Models\Langs::getDefaultLangCode()) !!}

<?php

if(! isset($canonical)){
    $urlCurrent = url()->current();

    $lng = request()->segment(1);

    if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
        $ruUrl = $urlCurrent;
        $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
    } else {
        $defaultUrl = $urlCurrent;
        $urlAfterDomain = request()->path();

        $r = '/ru/' . $urlAfterDomain;
        $r = str_replace('//','',$r);
        $ruUrl = env('APP_URL') . $r;
    }

    $canonical = "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>";
}

echo $canonical ?? '';

echo '<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Y1QXVJWD3B"></script>
';
echo "<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Y1QXVJWD3B');
</script>";
?>
