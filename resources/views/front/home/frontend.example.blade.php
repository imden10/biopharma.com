<!DOCTYPE html>
<html lang="{{config('translatable.locales_lang_codes')[app()->getLocale()]}}">
<head>@include('front.home.meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <link rel="icon" href="/assets/app/favicon.ico">
    <link href="/assets/app/css/ContactsPage.e28a64b3.css" rel="prefetch">
    <link href="/assets/app/css/ContactsPage~form.73f8d796.css" rel="prefetch">
    <link href="/assets/app/css/Page404.6c2e5d8d.css" rel="prefetch">
    <link href="/assets/app/css/ProductItem.9b898a17.css" rel="prefetch">
    <link href="/assets/app/css/Products.0862d7b3.css" rel="prefetch">
    <link href="/assets/app/css/article.7205668a.css" rel="prefetch">
    <link href="/assets/app/css/form.04611989.css" rel="prefetch">
    <link href="/assets/app/css/landing.e884244a.css" rel="prefetch">
    <link href="/assets/app/css/news.1499a99a.css" rel="prefetch">
    <link href="/assets/app/css/news~search.a2e5b6f9.css" rel="prefetch">
    <link href="/assets/app/css/search.86be437a.css" rel="prefetch">
    <link href="/assets/app/js/ContactsPage.e4a1ef5d.js" rel="prefetch">
    <link href="/assets/app/js/ContactsPage~form.9858d0c0.js" rel="prefetch">
    <link href="/assets/app/js/Page404.ddaed2ff.js" rel="prefetch">
    <link href="/assets/app/js/ProductItem.3b544b79.js" rel="prefetch">
    <link href="/assets/app/js/Products.7235127f.js" rel="prefetch">
    <link href="/assets/app/js/article.ac6896dc.js" rel="prefetch">
    <link href="/assets/app/js/form.3b8e19a2.js" rel="prefetch">
    <link href="/assets/app/js/landing.bc380363.js" rel="prefetch">
    <link href="/assets/app/js/news.adc1b8bb.js" rel="prefetch">
    <link href="/assets/app/js/news~search.406160dd.js" rel="prefetch">
    <link href="/assets/app/js/search.1e51eef0.js" rel="prefetch">
    <link href="/assets/app/css/app.15bb638a.css" rel="preload" as="style">
    <link href="/assets/app/css/chunk-vendors.a79fb5e8.css" rel="preload" as="style">
    <link href="/assets/app/js/app.70cc98ec.js" rel="preload" as="script">
    <link href="/assets/app/js/chunk-vendors.80c5e263.js" rel="preload" as="script">
    <link href="/assets/app/css/chunk-vendors.a79fb5e8.css" rel="stylesheet">
    <link href="/assets/app/css/app.15bb638a.css" rel="stylesheet">
</head>
<body>
<noscript><strong>We're sorry but biofarma_vue doesn't work properly without JavaScript enabled. Please enable it to
        continue.</strong></noscript>
<div id="app"></div>
<script src="/assets/app/js/chunk-vendors.80c5e263.js"></script>
<script src="/assets/app/js/app.70cc98ec.js"></script>
</body>
</html>
