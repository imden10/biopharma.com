<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name_{{ $lang }}">Название</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][name]" value="{{ old('page_data.' . $lang . '.name', $data[$lang]['name'] ?? '') }}" id="page_name_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.name') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_description_{{ $lang }}">Инструкция</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][description]"
            id="page_description_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.description') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.description', $data[$lang]['description'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.description') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group row field-type field-type-2">--}}
{{--    <label class="col-md-3 text-right" for="page_instruction_{{ $lang }}">Инструкция PDF</label>--}}
{{--    <div class="col-md-9">--}}
{{--        {{ file_preview_box("page_data[".$lang."][instruction]",old('page_data.' . $lang . '.instruction', $data[$lang]['instruction'] ?? '')) }}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_release_form_{{ $lang }}">Форма выпуску</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <input type="text" name="page_data[{{ $lang }}][release_form]" value="{{ old('page_data.' . $lang . '.release_form', $data[$lang]['release_form'] ?? '') }}" id="page_release_form_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.release_form') ? ' is-invalid' : '' }}">--}}

{{--        @if ($errors->has('page_data.' . $lang . '.release_form'))--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                <strong>{{ $errors->first('page_data.' . $lang . '.release_form') }}</strong>--}}
{{--            </span>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}

<hr>
<h4>SEO</h4>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_title_{{ $lang }}">Meta title</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_title]" value="{{ old('page_data.' . $lang . '.meta_title', $data[$lang]['meta_title'] ?? '') }}" id="page_meta_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_description_{{ $lang }}">Meta description</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_description]" value="{{ old('page_data.' . $lang . '.meta_description', $data[$lang]['meta_description'] ?? '') }}" id="page_meta_description_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_description') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_description') }}</strong>
            </span>
        @endif
    </div>
</div>

<hr>

@push('styles')

@endpush

@push('scripts')
    <script>
        $(document).ready(() => {

        });
    </script>
@endpush
