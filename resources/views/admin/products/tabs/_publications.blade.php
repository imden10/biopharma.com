<span class="btn btn-success text-white add-images-for-gallery" data-lang="{{$lang}}" style="margin-bottom: 15px">Добавить</span>

<div class="publications-container-{{$lang}}">
    <div class="input-group mb-1">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template-publications item-group input-group mb-1">
                <label class="input-file-label" style="width: 300px"></label>
                <input type="text" placeholder="ФИО" name="page_data[{{ $lang }}][publication][#dynamicListPlaceholder][name]" class="form-control mr-1" disabled="">
                <input type="text" placeholder="Название" name="page_data[{{ $lang }}][publication][#dynamicListPlaceholder][title]" class="form-control mr-1" disabled="">
                <input type="hidden" name="page_data[{{ $lang }}][publication][#dynamicListPlaceholder][file]" class="form-control mr-1 input-file-name" disabled="">
                <div class="input-group-append">
                    <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="publication" value="">

        <?php $dataPublications = isset($data[$lang]['publication']) ? json_decode($data[$lang]['publication'],true) : [];?>
        <div class="items-container-publications w-100">
            @foreach($dataPublications as $k => $value)
                <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                    <?php
                        $parts = explode('/',$value['file']);
                        if(is_array($parts)){
                            $file = $parts[count($parts) - 1];
                        } else {
                            $file = '';
                        }
                    ?>
                    <label style="width: 300px">{{$file}}</label>
                    <input type="text" placeholder="ФИО" name="page_data[{{ $lang }}][publication][{{$k}}][name]" value="{{$value['name']}}" class="form-control mr-1">
                    <input type="text" placeholder="Название" name="page_data[{{ $lang }}][publication][{{$k}}][title]" value="{{$value['title']}}" class="form-control mr-1">
                    <input type="hidden" name="page_data[{{ $lang }}][publication][{{$k}}][file]" value="{{$value['file']}}" class="form-control mr-1">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</div>

@push('styles')

@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $(".add-images-for-gallery").on('click', function () {
                let lang = $(this).data('lang');
                window.open('/filemanager?type=file', 'FileManager', 'width=900,height=600');
                window.SetUrl = function( url ) {
                    if(url.length){
                        let urls = [];
                        for(let i in url){

                            urls.push(url[i].url);

                            $(".publications-container-" + lang).find(".input-file-name").val(url[i].url);
                            $(".publications-container-" + lang).find(".input-file-label").text(url[i].name);

                            const template = $(".publications-container-" + lang).find('.item-template-publications');

                            const container = $(".publications-container-" + lang).find('.items-container-publications');

                            create_item(template, container, '#dynamicListPlaceholder');

                            container.find('input, textarea').prop('disabled', false);
                            container.find('.input-file-name').removeClass('input-file-name');
                            container.find('.input-file-label').removeClass('input-file-label');

                        }
                    }
                };
            });
        });
    </script>
@endpush
