
<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right">Похожие товары</label>
    <div class="col-md-9">
        <?php
            $sel = \App\Models\ProductsSimilar::query()->where('self_id',$model->id)->pluck('similar_id')->toArray();
        ?>
        <select name="similar_products[]" multiple class="select2-field" style="width: 100%">
            @foreach(\App\Models\Products::query()->active()->get() as $item)
                <option value="{{$item->id}}" @if(in_array($item->id,$sel)) selected @endif>{{$item->name}}</option>
            @endforeach
        </select>
    </div>
</div>

@push('styles')

@endpush

@push('scripts')
    <script>
        $(document).ready(() => {

        });
    </script>
@endpush
