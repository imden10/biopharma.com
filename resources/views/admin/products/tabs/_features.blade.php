<h4>Характеристики</h4>

<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right" for="page_instruction_{{ $lang }}">Інструкція PDF</label>
    <div class="col-md-9">
        {{ file_preview_box("page_data[".$lang."][instruction]",old('page_data.' . $lang . '.instruction', $data[$lang]['instruction'] ?? '')) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_composition_{{ $lang }}">Склад</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][composition]" value="{{ old('page_data.' . $lang . '.composition', $data[$lang]['composition'] ?? '') }}" id="page_composition_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.composition') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.composition'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.composition') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_release_form_{{ $lang }}">Лікарська форма</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][release_form]" value="{{ old('page_data.' . $lang . '.release_form', $data[$lang]['release_form'] ?? '') }}" id="page_release_form_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.release_form') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.release_form'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.release_form') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_age_{{ $lang }}">Термін придатності</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][age]" value="{{ old('page_data.' . $lang . '.age', $data[$lang]['age'] ?? '') }}" id="page_age_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.age') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.age'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.age') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_storage_conditions_{{ $lang }}">Умови зберігання</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][storage_conditions]" value="{{ old('page_data.' . $lang . '.storage_conditions', $data[$lang]['storage_conditions'] ?? '') }}" id="page_storage_conditions_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.storage_conditions') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.storage_conditions'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.storage_conditions') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right">Упаковка</label>
    <div class="col-md-9">
        <div class="input-group mb-1">
            <div style="display: none;">
                <div data-item-id="#dynamicListPlaceholder" class="item-template-package item-group input-group mb-1">
                    <input type="text" name="page_data[{{ $lang }}][package][#dynamicListPlaceholder][val]" class="form-control mr-1" disabled="">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="package" value="">

            <?php
            $dataPackage = isset($data[$lang]['package']) ? json_decode($data[$lang]['package'],true) : [];
            ?>
            <div class="items-container-package w-100">
                @foreach($dataPackage as $k => $value)
                    <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                        <input type="text" name="page_data[{{ $lang }}][package][{{$k}}][val]" value="{{$value['val']}}" class="form-control mr-1">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <button type="button" class="btn btn-info btn-sm add-item-package-{{$lang}}">Добавить</button>
    </div>
</div>

<hr>
<h4>Показания для вывода в каталоге</h4>

<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right">Показания для вывода в каталоге</label>
    <div class="col-md-9">
        <div class="input-group mb-1">
            <div style="display: none;">
                <div data-item-id="#dynamicListPlaceholder" class="item-template-indication-catalog item-group input-group mb-1">
                    <input type="text" name="page_data[{{ $lang }}][catalogIndication][#dynamicListPlaceholder][val]" class="form-control mr-1" disabled="">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="catalogIndication" value="">

            <?php $dataIndicationCatalog = isset($data[$lang]['catalogIndication']) ? json_decode($data[$lang]['catalogIndication'],true) : [];?>
            <div class="items-container-indication-catalog w-100">
                @foreach($dataIndicationCatalog as $k => $value)
                    <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                        <input type="text" name="page_data[{{ $lang }}][catalogIndication][{{$k}}][val]" value="{{$value['val']}}" class="form-control mr-1">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <button type="button" class="btn btn-info btn-sm add-item-indication-catalog-{{$lang}}">Добавить</button>
    </div>
</div>

<hr>
<h4>Показания</h4>

<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right">Показания</label>
    <div class="col-md-9">
        <div class="input-group mb-1">
            <div style="display: none;">
                <div data-item-id="#dynamicListPlaceholder" class="item-template-indication item-group input-group mb-1">
                    <input type="text" name="page_data[{{ $lang }}][indication][#dynamicListPlaceholder][val]" class="form-control mr-1" disabled="">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="indication" value="">

            <?php $dataIndication = isset($data[$lang]['indication']) ? json_decode($data[$lang]['indication'],true) : [];?>
            <div class="items-container-indication w-100">
                @foreach($dataIndication as $k => $value)
                    <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                        <input type="text" name="page_data[{{ $lang }}][indication][{{$k}}][val]" value="{{$value['val']}}" class="form-control mr-1">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <button type="button" class="btn btn-info btn-sm add-item-indication-{{$lang}}">Добавить</button>
    </div>
</div>

<hr>
<h4>Противопоказания</h4>

<div class="form-group row field-type field-type-2">
    <label class="col-md-3 text-right">Противопоказания</label>
    <div class="col-md-9">
        <div class="input-group mb-1">
            <div style="display: none;">
                <div data-item-id="#dynamicListPlaceholder" class="item-template-contraindications item-group input-group mb-1">
                    <input type="text" name="page_data[{{ $lang }}][contraindications][#dynamicListPlaceholder][val]" class="form-control mr-1" disabled="">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="contraindications" value="">

            <?php $dataContraindications = isset($data[$lang]['contraindications']) ? json_decode($data[$lang]['contraindications'],true) : [];?>
            <div class="items-container-contraindications w-100">
                @foreach($dataContraindications as $k => $value)
                    <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                        <input type="text" name="page_data[{{ $lang }}][contraindications][{{$k}}][val]" value="{{$value['val']}}" class="form-control mr-1">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <button type="button" class="btn btn-info btn-sm add-item-contraindications-{{$lang}}">Добавить</button>
    </div>
</div>
@push('styles')

@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $(document).on('click','.add-item-package-{{$lang}}',function () {
                const template = $(this).parent().find('.item-template-package');
                const container = $(this).parent().find('.items-container-package');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click','.add-item-indication-{{$lang}}',function () {
                const template = $(this).parent().find('.item-template-indication');
                const container = $(this).parent().find('.items-container-indication');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click','.add-item-contraindications-{{$lang}}',function () {
                const template = $(this).parent().find('.item-template-contraindications');
                const container = $(this).parent().find('.items-container-contraindications');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click','.add-item-indication-catalog-{{$lang}}',function () {
                const template = $(this).parent().find('.item-template-indication-catalog');
                const container = $(this).parent().find('.items-container-indication-catalog');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click', '.remove-item', function () {
                $(this).parents('.item-group').remove();
            });
        });
    </script>
@endpush
