<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <h4>Форма на странице продуктов</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 1</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_products_text1]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_products_text1', $data[$key]['form_products_text1'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 2</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_products_text2]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_products_text2', $data[$key]['form_products_text2'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>
            <hr>

            <h4>Для специалистов</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 1</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_pharmakanyaglyad_text1]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_pharmakanyaglyad_text1', $data[$key]['form_pharmakanyaglyad_text1'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 2</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_pharmakanyaglyad_text2]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_pharmakanyaglyad_text2', $data[$key]['form_pharmakanyaglyad_text2'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 3</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_text3]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_text3', $data[$key]['form_pharmakanyaglyad_text3'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_text3{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_text3') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Meta Title</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_title]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_title', $data[$key]['form_pharmakanyaglyad_title'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_title{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Meta Description</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_description]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_description', $data[$key]['form_pharmakanyaglyad_description'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_description{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_description') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>

            <h4>Для потребителей</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 1</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_pharmakanyaglyad_default_text1]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_pharmakanyaglyad_default_text1', $data[$key]['form_pharmakanyaglyad_default_text1'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 2</label>
                <div class="col-md-9">
                    <textarea name="setting_data[{{ $key }}][form_pharmakanyaglyad_default_text2]" class="form-control summernote">{!! old('setting_data.' . $key . '.form_pharmakanyaglyad_default_text2', $data[$key]['form_pharmakanyaglyad_default_text2'][0]['value'] ?? '') !!}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Текст 3</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_default_text3]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_default_text3', $data[$key]['form_pharmakanyaglyad_default_text3'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_default_text3{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_default_text3') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Meta Title</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_default_title]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_default_title', $data[$key]['form_pharmakanyaglyad_default_title'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_default_title{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_default_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Meta Description</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_pharmakanyaglyad_default_description]" value="{{ old('setting_data.' . $key . '.form_pharmakanyaglyad_default_description', $data[$key]['form_pharmakanyaglyad_default_description'][0]['value'] ?? '') }}" id="setting_form_pharmakanyaglyad_default_description{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_pharmakanyaglyad_default_description') ? ' is-invalid' : '' }}">
                </div>
            </div>
        </div>
    @endforeach
</div>


@push('scripts')
    <script type="text/javascript">
        $('.tab-content').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });
    </script>
@endpush
