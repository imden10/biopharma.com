<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <h4>Ссылка на форму записи</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_sing_link_title{{ $key }}">Текст ссылки</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][sing_link_title]" value="{{ old('setting_data.' . $key . '.sing_link_title', $data[$key]['sing_link_title'][0]['value'] ?? '') }}" id="setting_sing_link_title{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.sing_link_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_sing_link_{{ $key }}">Url</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][sing_link]" value="{{ old('setting_data.' . $key . '.sing_link', $data[$key]['sing_link'][0]['value'] ?? '') }}" id="setting_sing_link_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.sing_link') ? ' is-invalid' : '' }}">
                </div>
            </div>
            <hr>
        </div>
    @endforeach
</div>


<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_default_og_image_{{ $defaultLang }}">Логотип сайта</label>
    <div class="col-md-9">
        {{ media_preview_box("setting_data[".$defaultLang."][default_og_image]",old('setting_data.' . $defaultLang . '.default_og_image', $data[$defaultLang]['default_og_image'][0]['value'] ?? '')) }}
    </div>
</div>
