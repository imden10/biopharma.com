<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <h4>Форма подписки</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form_title]" value="{{ old('setting_data.' . $key . '.footer_form_title', $data[$key]['footer_form_title'][0]['value'] ?? '') }}" id="setting_footer_form_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form_placeholder_{{ $key }}">Подсказка e-mail</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form_placeholder]" value="{{ old('setting_data.' . $key . '.footer_form_placeholder', $data[$key]['footer_form_placeholder'][0]['value'] ?? '') }}" id="setting_footer_form_placeholder_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form_placeholder') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <h4>Ссылка</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_link_text_{{ $key }}">Текст</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_link_text]" value="{{ old('setting_data.' . $key . '.footer_link_text', $data[$key]['footer_link_text'][0]['value'] ?? '') }}" id="setting_footer_link_text_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_link_text') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_link_href_{{ $key }}">Url</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_link_href]" value="{{ old('setting_data.' . $key . '.footer_link_href', $data[$key]['footer_link_href'][0]['value'] ?? '') }}" id="setting_footer_link_href_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_link_href') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_copyright_{{ $key }}">Копирайт</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][copyright]" value="{{ old('setting_data.' . $key . '.copyright', $data[$key]['copyright'][0]['value'] ?? '') }}" id="setting_copyright_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.copyright') ? ' is-invalid' : '' }}">
                </div>
            </div>
        </div>
    @endforeach
</div>

<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<h4>Соц. сети</h4>

<div class="form-group row">
    <label class="col-md-3 text-right"></label>
    <div class="col-md-9">
        <div class="input-group mb-1">
            <div style="display: none;">
                <div data-item-id="#dynamicListPlaceholder" class="item-link-template-none item-group input-group mb-1">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" placeholder="Посилання" name="setting_data[{{ $defaultLang }}][links][#dynamicListPlaceholder][link]" class="form-control" disabled="">
                        </div>
                        <div class="col-md-5">
                            <select name="setting_data[{{ $defaultLang }}][links][#dynamicListPlaceholder][icon]" class="form-control">
                                <option value="">---</option>
                                <option value="ic-icfb">Facebook</option>
                                <option value="ic-icyoutube">Youtube</option>
                                <option value="ic-icinst">Instagram</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="setting_data[{{ $defaultLang }}][links]" value="">

            <?php $dataLinks = isset($data[$defaultLang]['links'][0]['value']) ? json_decode($data[$defaultLang]['links'][0]['value'],true) : [];?>

            <div class="items-link-container w-100">
                @foreach($dataLinks as $k => $value)
                    <div data-item-id="{{$defaultLang}}" class="item-template item-group input-group mb-1">
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" placeholder="Посилання" name="setting_data[{{ $defaultLang }}][links][{{$k}}][link]" value="{{$value['link']}}" class="form-control">
                            </div>
                            <div class="col-md-5">
                                <select name="setting_data[{{ $defaultLang }}][links][{{$k}}][icon]" class="form-control">
                                    <option value="">---</option>
                                    <option value="ic-icfb" @if($value['icon'] === 'ic-icfb') selected @endif >Facebook</option>
                                    <option value="ic-icyoutube" @if($value['icon'] === 'ic-icyoutube') selected @endif >Youtube</option>
                                    <option value="ic-icinst" @if($value['icon'] === 'ic-icinst') selected @endif >Instagram</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <button type="button" class="btn btn-info btn-sm add-item-link">Добавить</button>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click','.add-item-link',function () {
                const template = $(this).parent().find('.item-link-template-none');
                const container = $(this).parent().find('.items-link-container');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click', '.remove-item', function () {
                $(this).parents('.item-group').remove();
            });
        });
    </script>
@endpush
