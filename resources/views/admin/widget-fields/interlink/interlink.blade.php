<div class="input-group interlink-container">
    <div class="input-group-prepend">
        <span class="input-group-text" id="">{{$label ?? 'Ссылка'}}</span>
    </div>
    <select class="form-control interlink-select-type" name="{{$name_type}}">
        <option value="page" @if(isset($sel_type) && $sel_type == 'page') selected @endif>Страница</option>
        <option value="article" @if(isset($sel_type) && $sel_type == 'article') selected @endif>Новость</option>
        <option value="arbitrary" @if(isset($sel_type) && $sel_type == 'arbitrary') selected @endif>Произвольная ссылка</option>
    </select>
    <div class="select-type select-type-page" @if(!isset($sel_type) || (isset($sel_type) && $sel_type == 'page')) style="display: initial;" @else style="display: none;" @endif>
        <select class="form-control select2-field" style="width: 300px" name="{{$name_val}}[page]">
            <option value="">---</option>
            @foreach(\App\Models\Pages::query()->active()->get() as $item)
                <option value="{{$item->id}}" @if(isset($sel_type) && $sel_type == 'page' && $sel_val[$sel_type] == $item->id) selected @endif>{{$item->title}}</option>
            @endforeach
        </select>
    </div>
   <div class="select-type select-type-article" @if(isset($sel_type) && $sel_type == 'article') style="display: initial;" @else style="display: none;" @endif>
       <select class="form-control select2-field" style="width: 300px" name="{{$name_val}}[article]">
           <option value="">---</option>
           @foreach(\App\Models\BlogArticles::query()->active()->get() as $item)
               <option value="{{$item->id}}" @if(isset($sel_type) && $sel_type == 'article' && $sel_val[$sel_type] == $item->id) selected @endif title="{{$item->name}}">{{mb_strimwidth($item->name, 0, 30, "...")}}</option>
           @endforeach
       </select>
   </div>
    <input type="text" class="form-control select-type select-type-arbitrary" @if(isset($sel_type) && $sel_type == 'arbitrary') value="{{$sel_val[$sel_type]}}" @endif  @if(isset($sel_type) && $sel_type == 'arbitrary') style="display: initial;" @else style="display: none;" @endif name="{{$name_val}}[arbitrary]">
</div>
