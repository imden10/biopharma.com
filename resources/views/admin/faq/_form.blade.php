<div class="form-group row">
    <label class="col-md-3 text-right" for="page_question_{{ $lang }}">Вопрос</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][question]" value="{{ old('page_data.' . $lang . '.question', $data[$lang]['question'] ?? '') }}" id="page_question_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.question') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.question'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.question') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_answer_{{ $lang }}">Ответ</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][answer]"
            id="page_answer_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.answer') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.answer', $data[$lang]['answer'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.answer'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.answer') }}</strong>
            </span>
        @endif
    </div>
</div>


