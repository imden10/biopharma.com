@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item"><a href="{{route('faq.index')}}">Частые вопросы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <form class="form-horizontal" method="POST" action="{{route('faq.update', $model->id)}}">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($localizations as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link @if(app()->getLocale() == $key) active @endif"
                                       data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                        <span class="hidden-sm-up"></span> <span
                                            class="hidden-xs-down">{{ $lang }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                        <br>
                        <div class="tab-content">
                            @foreach($localizations as $key => $catLang)
                                <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
                                     id="main_lang_{{ $key }}" role="tabpanel">
                                    @include('admin.faq._form',[
                                       'lang'  => $key,
                                       'model' => $model,
                                       'data'  => $data
                                    ])
                                </div>
                            @endforeach

                            <div class="form-group row">
                                <label class="col-md-3 text-right" for="page_audio_id">Аудио трек</label>
                                <div class="col-md-9">
                                    <select name="audio_id" id="audio_id" class="select-field" style="width: 100%">
                                        <option value="">---</option>
                                        @foreach(\App\Models\Audio::query()->get() as $item)
                                            <option value="{{$item->id}}" @if(old('audio_id', $model->audio_id ?? '') == $item->id) selected @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.select-field').select2();
        })
    </script>
@endpush
