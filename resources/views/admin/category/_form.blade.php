<div class="form-group row">
    <label class="col-md-3 text-right" for="page_title_{{ $lang }}">Название</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][title]" value="{{ old('page_data.' . $lang . '.title', $data[$lang]['title'] ?? '') }}" id="page_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_h1_{{ $lang }}">H1</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][h1]" value="{{ old('page_data.' . $lang . '.h1', $data[$lang]['h1'] ?? '') }}" id="page_h1_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.h1') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.h1'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.h1') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_description_{{ $lang }}">Описание</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <textarea--}}
{{--            name="page_data[{{ $lang }}][description]"--}}
{{--            id="page_description_{{ $lang }}"--}}
{{--            class="summernote editor {{ $errors->has('page_data.' . $lang . '.description') ? ' is-invalid' : '' }}"--}}
{{--            cols="30"--}}
{{--            rows="10"--}}
{{--        >{{ old('page_data.' . $lang . '.description', $data[$lang]['description'] ?? '') }}</textarea>--}}

{{--        @if ($errors->has('page_data.' . $lang . '.description'))--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                <strong>{{ $errors->first('page_data.' . $lang . '.description') }}</strong>--}}
{{--            </span>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_title_{{ $lang }}">Meta title</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_title]" value="{{ old('page_data.' . $lang . '.meta_title', $data[$lang]['meta_title'] ?? '') }}" id="page_meta_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_title') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_meta_keywords_{{ $lang }}">Meta keywords</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <input type="text" name="page_data[{{ $lang }}][meta_keywords]" value="{{ old('page_data.' . $lang . '.meta_keywords', $data[$lang]['meta_keywords'] ?? '') }}" id="page_meta_keywords_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_keywords') ? ' is-invalid' : '' }}">--}}

{{--        @if ($errors->has('page_data.' . $lang . '.meta_keywords'))--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                <strong>{{ $errors->first('page_data.' . $lang . '.meta_keywords') }}</strong>--}}
{{--            </span>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_description_{{ $lang }}">Meta description</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_description]" value="{{ old('page_data.' . $lang . '.meta_description', $data[$lang]['meta_description'] ?? '') }}" id="page_meta_description_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_description') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_description') }}</strong>
            </span>
        @endif
    </div>
</div>


