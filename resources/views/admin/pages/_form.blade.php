<div class="form-group row">
    <label class="col-md-3 text-right" for="parent_id">Родительская страница</label>
    <div class="col-md-9">
        <select name="parent_id" class="select2-field" id="parent_id" style="width: 100%">
            <option value="0">---</option>
            @foreach(\App\Models\Pages::query()->active()->get() as $page)
                <option value="{{$page->id}}" @if($page->id == $model->parent_id) selected @endif>{{$page->title}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="type">Тип</label>
    <div class="col-md-9">
        <select name="type" class="select2-field" id="type" style="width: 100%">
            @foreach(\App\Models\Pages::getTypes() as $key => $type)
                <option value="{{$key}}" @if($key == $model->type) selected @endif>{{$type}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image',$model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if ($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>

        function getQueryVar(url,varName){
            var queryStr = unescape(url) + '&';
            var regex = new RegExp('.*?[&\\?]' + varName + '=(.*?)&.*');
            var val = queryStr.replace(regex, "$1");
            return val == queryStr ? false : val;
        }

        $(document).ready(function(){
            $('.select-field').select2();
        })
    </script>
@endpush
