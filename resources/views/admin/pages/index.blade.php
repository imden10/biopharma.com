@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Страницы</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('pages.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\Pages::getStatuses() as $key => $item)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$item['title']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <input type="text" class="form-control" name="title" value="{{old('title', request()->input('title'))}}">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('pages.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Заголовок</th>
                            <th>Родительская страница</th>
                            <th>URL</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>
                                    <a href="{{ route('pages.edit', $item->id) }}">
                                        {{$item->title}}
                                    </a>
                                </td>
                                <td>{{$item->getNameWithPath()}}</td>
                                <td>{{$item->slug}}</td>
                                <td>{!! $item->showStatus() !!}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('pages.destroy', $item->id) }}" method="POST">
                                                <?php
                                                    $frontLink = '/' . $item->path;
                                                    $frontLink = str_replace('//','/',$frontLink);
                                                ?>
                                                <a href="{{$frontLink}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info  btn-xs"><i class="fa fa-eye"></i></a>

                                                <a href="{{ route('pages.edit', $item->id) }}" class="btn btn-xs btn-primary">
                                                    <i class="fas fa-edit fa-lg"></i>
                                                </a>

                                                @csrf
                                                @method('DELETE')

                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
