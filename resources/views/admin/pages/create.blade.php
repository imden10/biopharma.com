@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('pages.index')}}">Страницы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавть</li>
        </ol>
    </nav>

    <form class="form-horizontal" method="POST" action="{{ route('pages.store') }}">
        @csrf

    <div class="row">
        <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                    <li class="nav-item">
                        <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                    </li>
                </ul>
            </div>

             <div class="card-body">
                 <div class="tab-content" id="myTabContent">
                     {{----------------------------- MAIN TAB -----------------------------------------}}
                     <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                         <ul class="nav nav-tabs" role="tablist">
                             @foreach($localizations as $key => $lang)
                                 <li class="nav-item">
                                     <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                         <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                     </a>
                                 </li>
                             @endforeach
                         </ul>

                         <br>
                         <div class="tab-content">
                             @foreach($localizations as $key => $catLang)
                                 <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}" role="tabpanel">
                                     @include('admin.pages.tabs._main',[
                                        'lang' => $key,
                                        'model' => $model
                                     ])

                                     {!! Constructor::output(\App\Models\Translations\PagesTranslation::class,$key) !!}
                                 </div>
                             @endforeach

                             @include('admin.pages._form',['model' => $model])
                         </div>
                     </div>

                     {{----------------------------- SEO TAB -----------------------------------------}}
                     <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                         <ul class="nav nav-tabs" role="tablist">
                             @foreach($localizations as $key => $lang)
                                 <li class="nav-item">
                                     <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#seo_lang_{{ $key }}" role="tab">
                                         <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                     </a>
                                 </li>
                             @endforeach
                         </ul>

                         <br>
                         <div class="tab-content tabcontent-border">
                             @foreach($localizations as $key => $catLang)
                                 <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="seo_lang_{{ $key }}" role="tabpanel">
                                     @include('admin.pages.tabs._seo',[
                                        'lang' => $key
                                     ])
                                 </div>
                             @endforeach
                         </div>
                     </div>
                 </div>

                 <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

             </div>
            </div>
        </div>
    </div>

    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(".select2-field").select2();
        });
    </script>
@endpush
