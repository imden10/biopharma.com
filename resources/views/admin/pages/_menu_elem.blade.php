<div class="menu-elem">
    <span class="title">{{$title}}</span>
    <div class="r">
        <span class="placeholder-type">{{$typeName}}</span>
        <span class="remove-btn" title="Удалить">
            <i class="fa fa-times"></i>
        </span>
    </div>

    <input type="hidden" name="menu[{{$randKey}}][id]" value="{{$modelId}}">
    <input type="hidden" name="menu[{{$randKey}}][class]" value="{{$modelClass}}">
    <input type="hidden" name="menu[{{$randKey}}][title]" value="{{$modelTitle}}">
    <input type="hidden" name="menu[{{$randKey}}][slug]" value="{{$modelSlug}}">
</div>
