@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item active" aria-current="page">Підписка</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card card-exportable">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th data-field="email">E-mail</th>
                        <th data-field="created_at">Дата</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>{{$item->email}}</td>
                                <td>{{$item->created_at->format('d.m.Y')}}</td>
                                <td>
                                    <form action="{{ route('subscribe.delete') }}" method="POST">
                                        <input type="hidden" name="id" value="{{$item->id}}">
                                        @csrf
                                        @method('DELETE')

                                        <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
    <script src="{{asset('/myLib/im-export-table-lib.js')}}"></script>
    <script>
        $(document).ready(function(){
            let exportLib = new imExportTableLib({
                cardQuerySelector: ".card-exportable",
                dbTable: "subscribes"
            });

            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверены?',
                    text: "Вы действительно хотите удалить запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да!',
                    cancelButtonText: 'Нет!'
                }).then((result) => {
                    if (result.value) {
                        _this.closest('form').submit();
                    }
                });
            });
        });
    </script>
@endpush
