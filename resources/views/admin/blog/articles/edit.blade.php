@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item"><a href="{{route('articles.index')}}">Публикации</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <div style="display: flex; justify-content: flex-end;margin-bottom: 10px;">
        @isset($model->tags[0]->slug)
            <a href="/news/{{$model->tags[0]->slug}}/{{$model->slug}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Посмотреть на сайте</a>
        @else
            <a href="#" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs disabled"><i class="fa fa-eye"></i> Посмотреть на сайте</a>
        @endisset
    </div>

    <form class="form-horizontal" method="POST" action="{{route('articles.update', $model->id)}}">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._main',[
                                               'lang' => $key,
                                               'data' => $data,
                                               'model' => $model
                                            ])
                                            {!! Constructor::output($model->getTranslation($key),$key) !!}
                                        </div>
                                    @endforeach

                                    @include('admin.blog.articles._form',['model' => $model])
                                </div>
                            </div>

                            {{----------------------------- SEO TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#seo_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content tabcontent-border">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="seo_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._seo',[
                                               'lang' => $key,
                                               'data' => $data
                                            ])
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".select2-field").select2();
        $(".select-field").select2();
    });
</script>
@endpush
