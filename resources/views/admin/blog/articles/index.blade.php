@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item active" aria-current="page">Публікації</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('articles.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\BlogArticles::getStatuses() as $key => $item)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Категория</label>
                            <select name="category" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\BlogTags::query()->get() as $item)
                                    <option value="{{$item->id}}" @if(old('category', request()->input('category')) == (string)$item->id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <input type="text" class="form-control" name="name" value="{{old('name', request()->input('name'))}}">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('articles.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Категория</th>
                        <th>Просмотры</th>
                        <th>Статус</th>
                        <th>Дата публикации</th>
                        <th style="width: 150px"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>
                                    <a href="{{ route('articles.edit', $item->id) }}">
                                        {{$item->name}}
                                    </a>
                                </td>
                                <td>{{$item->tags[0]->name ?? ''}}</td>
                                <td>{{$item->views}}</td>
                                <td>
                                    <span class="badge"
                                          style="color: {{\App\Models\BlogArticles::getStatusColors()[$item->status][1]}}; background-color:{{\App\Models\BlogArticles::getStatusColors()[$item->status][0]}}"
                                    >{{\App\Models\BlogArticles::getStatuses()[$item->status]}}</span>
                                </td>
                                <td>{{\Carbon\Carbon::create($item->public_date)->format('d-m-Y H:i')}}</td>
                                <td>
                                    <form action="{{ route('articles.destroy', $item->id) }}" method="POST">

                                        @isset($item->tags[0]->slug)
                                            <a href="/news/{{$item->tags[0]->slug}}/{{$item->slug}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info  btn-xs"><i class="fa fa-eye"></i></a>
                                        @else
                                            <a href="#" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs disabled"><i class="fa fa-eye"></i></a>
                                        @endisset

                                        <a href="{{ route('articles.edit', $item->id) }}" class="btn btn-xs btn-primary">
                                            <i class="fas fa-edit fa-lg"></i>
                                        </a>

                                        @csrf
                                        @method('DELETE')

                                        <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы уверены, что хотите удалить эту запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush

