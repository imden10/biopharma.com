<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Раздел</label>
    <div class="col-md-9">
        <?php
        $selectTags = \App\Models\BlogArticleTag::query()->where('blog_article_id',$model->id)->pluck('blog_tag_id')->toArray();
        ?>
        <select name="tags[]" id="tags" class="form-control select2">
            <option value="">---</option>
            @foreach(\App\Models\BlogTags::query()->get() as $tag)
                <option value="{{$tag->id}}" @if($selectTags && in_array($tag->id,$selectTags)) selected @endif>{{$tag->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image',$model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 text-right" for="public_date">
        Дата публикации</label>
    <div class="col-md-3">
        <input type="input" class="form-control" name="public_date" value="{{$model->public_date}}" id="public_date">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if ($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('matrix/css/icons/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('matrix/libs/moment/moment.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        function getQueryVar(url,varName){
            var queryStr = unescape(url) + '&';
            var regex = new RegExp('.*?[&\\?]' + varName + '=(.*?)&.*');
            var val = queryStr.replace(regex, "$1");
            return val == queryStr ? false : val;
        }

        $(document).ready(() => {
            $('.select2').select2();

            $('#public_date').datetimepicker({
                format: 'DD-MM-YYYY HH:mm',
                useCurrent: true,
                icons: {
                    time: "fa fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock",
                    clear: "fa fa-trash"
                }
            });

                @if($model->youtube)
            let url = "{{$model->youtube}}";
            let v = getQueryVar(url,'v');
            let link = 'https://img.youtube.com/vi/'+v+'/default.jpg';
            $('.youtube-preview').prop('src',link);
            @endif

            $("input[name='youtube']").on('change',function () {
                let v = getQueryVar($(this).val(),'v');

                let link = 'https://img.youtube.com/vi/'+v+'/default.jpg';

                $('.youtube-preview').prop('src',link);
            });
        });
    </script>
@endpush
