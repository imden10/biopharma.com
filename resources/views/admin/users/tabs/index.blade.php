<?php
use App\Models\User;

/* @var User $model */

?>
@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Користувачі</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$model->email}}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link @if($tab === 'main') active @endif" href="/admin/users/{{$model->id}}/main">Персональні дані</a>
                        </li>
                    </ul>
                    <br>

                    <div>
                        @include('admin.users.tabs._' . str_replace('-','_',$tab),[
                        'model' => $model
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
