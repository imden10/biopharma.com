<?php

use Illuminate\Support\Facades\Request;

?>

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">
                <li class="sidebar-item @if(in_array(Request::segment(1),['admin']) && Request::segment(2) == '') active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin')}}" aria-expanded="false">
                        <i class="mdi mdi-blur-linear"></i>
                        <span class="hide-menu">Панель приборов</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['pages'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('pages.index')}}" aria-expanded="false">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="hide-menu">Страницы</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-widgets"></i>
                        <span class="hide-menu">Продукты</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['category'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('category.index')}}" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Категории</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['products'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('products.index')}}" aria-expanded="false">
                                <i class="mdi mdi-tag"></i>
                                <span class="hide-menu">Продукты</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Блог</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['articles'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('articles.index')}}" aria-expanded="false">
                                <i class="mdi mdi-message-outline"></i>
                                <span class="hide-menu">Публикации</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['tags'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('tags.index')}}" aria-expanded="false">
                                <i class="mdi mdi-tag"></i>
                                <span class="hide-menu">Разделы</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['subscribe'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('subscribe.index')}}" aria-expanded="false">
                                <i class="mdi mdi-email"></i>
                                <span class="hide-menu">Подписки</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['faq'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('faq.index')}}" aria-expanded="false">
                        <i class="mdi mdi-comment-question-outline"></i>
                        <span class="hide-menu">Частые вопросы</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['users-admin'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users-admin.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Администраторы</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-format-paint"></i>
                        <span class="hide-menu">Внешний вид</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item @if(in_array(Request::segment(2),['widgets'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/widgets" aria-expanded="false">
                                <i class="mdi mdi-widgets"></i>
                                <span class="hide-menu">Виджеты</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(2),['menu'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('menu.index')}}" aria-expanded="false">
                                <i class="mdi mdi-menu"></i>
                                <span class="hide-menu">Меню</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-image-album"></i>
                        <span class="hide-menu">Мультимедиа</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['images'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('multimedia.images')}}" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Изображения</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(3),['files'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('multimedia.files')}}" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Файлы</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Настройки</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        @foreach(\App\Models\Settings::getTabNames() as $tabCode => $tabName)
                            <li class="sidebar-item @if(in_array(Request::segment(4),[$tabCode])) active @endif">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/{{$tabCode}}" aria-expanded="false">
                                    <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                    <span class="hide-menu">{{$tabName}}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
