<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('settings/all', [\App\Http\Controllers\Api\SettingsController::class,'getAll']);
Route::POST('get-main-page', [\App\Http\Controllers\Api\MainController::class,'getMain']);
Route::POST('page/get-by-slug', [\App\Http\Controllers\Api\PagesController::class,'getBySlug']);
Route::POST('menu/get-by-name', [\App\Http\Controllers\Api\MenuController::class,'getByName']);
Route::POST('menu/get-by-id', [\App\Http\Controllers\Api\MenuController::class,'getById']);
Route::POST('menu/get-by-ids', [\App\Http\Controllers\Api\MenuController::class,'getByIds']);
Route::POST('news/get-by-slug', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlug']);
Route::POST('news/all', [\App\Http\Controllers\Api\BlogArticlesController::class,'getAll']);
Route::POST('news/get-by-category-slug', [\App\Http\Controllers\Api\BlogArticlesController::class,'getByCategory']);
Route::POST('news/subscribe', [\App\Http\Controllers\Api\SubscribeController::class,'subscribe']);

Route::POST('products/get-by-slug', [\App\Http\Controllers\Api\ProductsController::class,'getBySlug']);
Route::POST('products/all', [\App\Http\Controllers\Api\ProductsController::class,'getAll']);
Route::POST('products/get-by-category-slug', [\App\Http\Controllers\Api\ProductsController::class,'getByCategory']);

Route::POST('request/feedback', [\App\Http\Controllers\Api\RequestController::class,'feedback']);
